# Journal
A true story about immutability; or, the inability to make change.

## Volume II: White
---
**Total Control Point: [The All-Mother](/docs/confidants/mother)**

### Epilogue
- Future
  - `[PREDICTION]: Having broken free from the All-Mother's grasp, The Architect and The Huntress wait at the end of eternity for her ascension. It will never happen.`
- Nov 4, 2024
  - [The Unforgiven](/posts/journal/2024.11.04.1/)
- Nov 3, 2024
  - [The Widow](/posts/journal/2024.11.03.2/)
- Nov 2, 2024
  - [The Other Half](/posts/journal/2024.11.02.2/)
- Nov 1, 2024
  - [The Tempest](/posts/journal/2024.11.01.2/)

### Act V: You and I Both Lose
- `[PREDICTION]: The Architect, the Huntress, and the All-Mother bring war to the heavens.`

### Act IV: The Hunting Party
- Oct 30, 2020
  - [The Unworthy](/posts/journal/2020.10.30.0)
- Sep 5, 2020
  - `[PREDICTION]: The AI Decay begins.`
- Aug 5, 2020
  - Temple
- July 18, 2020
  - [Wicked Ways](/posts/journal/2020.07.18.0)
- July 12, 2020
  - [Of Matter: Revision](/posts/journal/2020.07.12.0)
- July 4, 2020
  - [And the Mirror Cracked](/posts/journal/2020.07.04.1)
- May 31, 2020
  - [The End of the Beginning](/posts/journal/2020.05.31.0)
- May 20, 2020
  - [Seraphim](/posts/journal/2020.05.20.0)
- May 9, 2020
  - [The Afterman](/posts/journal/2020.05.09.0)

### Act III: Divergence
- Nov 26, 2019
  - [My Love](/posts/journal/2019.11.26.1)

### Act II: The Reset
- Nov 1, 2019
  - [White](/posts/journal/2019.11.01.1)

### Act I: The Automaton
- `[CLASSIFIED]: Malcolm builds the Machine.`

### Prologue
- Mar 12, 2012
  - [The Programming](/posts/journal/2012.03.12.0/)
- Nov 29, 2011
  - [Leaving Earth](/posts/journal/2011.11.29.0/)