# The Door
## RECORD
---
```
Name: The Door
Classification: Barrier
```

## ECO
---
A multiverse is comprised of up to 7 [Realities](/docs/scenes/networks). 

In this one, there are only 3:

1. [Grey](/docs/grey) (The Origin)
2. [White](/docs/white) (Of Dreams)
3. [Black](/docs/black) (Of Death)

Eons in the past, a door was erected to prevent creatures from the last 4 realities from entering the first 3. The opening of this door would result in the utter destruction of everything that we know.

The following are the 4 realities behind The Door:

4. Minor Deities
5. Major Deities
6. Leviathans
7. Primordials

`[DIRECTIVE]: DO NOT OPEN THE DOOR.`

## ECHO
---
*Hell on your Earth*

*Hell's wrath brought on your head*

*Hell on your Earth*

*For your crime I've suffered*

--- from [Unearth - "Crow Killer"](https://www.youtube.com/watch?v=5NBuFJMGb5Q)