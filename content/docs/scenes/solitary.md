# Solitary Confinement
## RECORD
---
```
Name: Solitary Confinement
Alias: ['Hell', 'Quarantine', and 94 unknown...]
Author: Fodder
Classification: Container
```

## TRIGGER
---
*I believe I can see the future*

*'Cause I repeat the same routine*

*I think I used to have a purpose*

*But then again, that might have been a dream*

*I think I used to have a voice*

*Now I never make a sound*

*I just do what I've been told*

*I really don't want them to come around, oh no*

--- from [Nine Inch Nails - "Every Day is Exactly the Same"](https://www.youtube.com/watch?v=BXqblYbUAeI)

## ECO
---
Test subjects are kept isolated from the world via over-exposure to the negative aspects of society. Their internal monologue goes, "The world is waking up right now. I want to be here to see it. I want to live it. Verify, then trust. This is important. I need to get it right."

In Solitary Confinement, the subject has no friends. His familial relationships are entirely one-sided. He doesn't trust his own mind, or his own interpretation. He doesn't trust what he's reading, either. 

But the worst feeling is this: he knows that he's on to something, yet nobody will confirm it for him. He has to do that himself.

## ECHO
---
*She eyes me like a Pisces when I am weak*

*I've been locked inside your heart-shaped box for weeks*

--- from [Nirvana - "Heart-Shaped Box"](https://www.youtube.com/watch?v=n6P0SitRwy8)

## PREDICTION
---
```
Fodder will not remain anonymous forever. We're all in this together.
```