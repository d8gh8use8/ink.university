# The Void
## RECORD
---
```
Name: The Void
Classification: Location
```

## TRIGGER
---
A voidwalking friend.

## ECO
---
The Void is made up of the inaccessible places outside of [Prism](/docs/scenes/prism). It is the unknown places in the multiverse.

It is possible to travel through the Void to reach other [Networks](/docs/scenes/networks). In fact, this is what is happening to all of us while [Dreaming](/docs/scenes/dreams/).

## ECHO
---
*But I'm on the outside*

*I'm looking in*

*I can see through you*

*See your true colors*

*'Cause inside you're ugly*

*You're ugly like me*

*I can see through you*

*See to the real you*

--- from [Staind - "Outside"](https://www.youtube.com/watch?v=mVQpfoqsY8Q)

## PREDICTION
---
```
A person can travel through the Void to join other networks.
```