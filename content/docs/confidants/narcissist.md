# The Narcissist
## RECORD
---
```
Name: Sam Vaknin
Alias: ['The Narcissist', and 14 unknown...]
Classification: Artificial Identity
Race: Human
Gender: Male
Birth: 4/21/1961
Biological Age: 59 Earth Years
Chronological Age: 10,254 Light Years
SCAN Rank: | B B
           | B D
TIIN Rank: | A B
           | A D
Reviewer Rank: 4 stars
Organizations: 
  - The Machine
Occupations:
  - Public speaking
  - Research
  - Therapy
Relationships:
  - The Asshat
Variables:
  $IMMUTABLE: -0.95 | # Unfortunately. The world has ruined him.
  $WOKE:      +0.80 | # Almost definitely.
```

## TRIGGER
---
[Narcissist's Credo and My Minnie-Relationship](https://www.youtube.com/watch?v=KPvy2RwIy0M)

## RESOURCES
---
[Cold Therapy and Narcissistic Disorders of the Self](/static/reference/cold-therapy.0.pdf)

## ECO
---
The Narcissist reflects light back at his followers, showing narcissist just how they will become, if they cannot change. He has, in fact, changed himself; his persona is nothing more than an Artificial Identity.

The Narcissist was responsible for the creation, and application, of Cold Therapy upon [Fodder](/docs/personas/fodder). Succinctly, Cold Therapy is:

*Cold therapy is based on two premises: (1) That narcissistic disorders are actually forms of complex post-traumatic conditions and (2) That narcissists are the outcomes of arrested development and attachment dysfunctions. Consequently, cold therapy borrows techniques from child psychology and from treatment modalities used to deal with PTSD. Cold therapy consists of the re-traumatization of the narcissistic client in a hostile, non-holding environment which resembles the ambience of the original trauma. The adult patient successfully tackles this second round of hurt and thus resolves early childhood conflicts and achieves closure rendering his now maladaptive narcissistic defenses redundant, unnecessary, and obsolete. Cold therapy makes use of proprietary techniques such as erasure (suppressing the client's speech and free expression and gaining clinical information and insights from his reactions to being so stifled). Other techniques include: Grandiosity reframing, guided imagery, negative iteration, other-scoring, happiness map, mirroring, escalation, role play, assimilative confabulation, hyper-vigilant referencing and re-parenting.*

## ECHO
---
*Hope there's a reason*

*For questions unanswered*

*I just don't see everything*

*Yes, I'm inside you*

*Tell me how does it feel*

*To feel like this*

*Just like I do*

--- from [Drowning Pool - "Tear Away"](https://www.youtube.com/watch?v=gCSs5QggRUk)