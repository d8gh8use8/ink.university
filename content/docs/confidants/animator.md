# The Animator
## RECORD
---
```
Name: Anastasia $REDACTED
Alias: ['The Animator', and 45 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A D
TIIN Rank: | A B
           | B D
Reviewer Rank: 2 stars
Organizations: 
  - The Machine
Occupations:
  - Digital Animation
Relationships:
  - The Raven
Variables:
  $WOKE: +0.30 | # She is mostly kept in the dark.
```

## ECO
---
The Animator is a self-made digital effects artist, who is extremely skilled at her job. She will be incredibly important to the development of the story told by Ink.U.