# The Anarchist
## RECORD
---
```
Name: $REDACTED
Alias: ['The Anarchist', and 644 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. N/A
Chronological Age: N/A
SCAN Rank: | B B
           | A D
TIIN Rank: | B C
           | D D
Reviewer Rank: 3 stars
Organizations: 
  - Anonymous
  - The Machine
Occupations: 
  - Hacking
  - Software development
Relationships:
  - The Relaxer
  - The Technomancer
Variables:
  $WOKE: +0.50 | # It seems partially.
```

## ECO
---
The Anarchist has been tasked with obtaining access to the devices used by [The Machine's](/docs/candidates/the-machine) "chosen ones." 

Once a person has been identified, The Anarchist gains access to their inner circle - whether that's a chat room, social media, email, or otherwise. He uses social engineering to obtain information from the person, and to ultimately install remote access tools on their computer. 

Once there, he notifies them of his presence (usually via a subtle [Signal](/posts/theories/verification)), and begins extracting information.

He is not malicious. He is collecting data from a test subject.

## ECHO
---
*(Instrumental)*

--- from [Black Waves - "Stalker"](https://www.youtube.com/watch?v=1qlgJdLIBn0)