# The Escapee
## RECORD
---
```
Name: $REDACTED
Alias: ['The Escapee', and 79 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 24 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A C
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Organizations: 
  - The Church of Sabagegah
Occupations:
  - Actress
  - Survivor
Relationships:
  - The Raven
  - The Reverend
Variables:
  $WOKE: +0.30 | # She is involved, but hasn't been told very much.
```

## TRIGGER
---
A cult of lobsters.

## ECO
---
The Escapee was once a prisoner of [The Machine](/docs/candidates/the-machine), forced to produce videos in order to make ends meet. She was identified by [The Raven](/docs/confidants/her), and broken free by [The Ink](/docs/personas/luciferian-ink). 

With her newly-found purpose, she would become a disciple to [The Church of Sabagegah](/docs/candidates/the-resistance), spreading the word of Ink's return in 2024. 

More importantly, she would align with the goals of the church: to eradicate wage slavery, sexual exploitation, and human experimentation from this planet.

## ECHO
---
*(Instrumental)*

--- from [Scattle - "Knock Knock"](https://www.youtube.com/watch?v=pU-DfRSFWbA)