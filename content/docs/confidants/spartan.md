# The Spartan
## RECORD
---
```
Name: $REDACTED
Alias: ['The Spartan', and 49 unknown...]
Classification: Artificial Identity
Race: Human
Gender: Male
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C A
           | B D
Reviewer Rank: 1 stars
Organizations: 
  - The Resistance
Variables:
  $WOKE: +0.40 | # Partially.
```

## ECO
---
The Spartan lives well-below his means. He does not care of the world's luxuries, preferring to live in relative modesty and solitude, using what little resources he has to give back to the world at-large.

The Spartan is the ideal representation of what all humans should strive to be.

## ECHO
---
*Anywhere you go, I'll follow you down*

*Anyplace but those I know by heart*

*Anywhere you go, I'll follow you down*

*I'll follow you down, but not that far*

--- from [Gin Blossoms - "Follow You Down"](https://www.youtube.com/watch?v=5LuH0ywYVQc)

## PREDICTION
---
```
He is building an army.
```