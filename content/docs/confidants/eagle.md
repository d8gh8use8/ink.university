# The Eagle
## RECORD
---
```
Name: Devin Stone
Alias: ['The Eagle', and 11 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 40 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 2 stars
Occupations:
  - Actor
  - Lawyer
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## ECO
---
The Eagle is [Fodder's](/docs/personas/fodder) lawyer. He will speak openly after the events of October 30, 2020.