# The Dave
## RECORD
---
```
Name: David Lejeune
Alias: ['Commissioner Gordon', 'God', 'The Cockroach King', 'The Dave', 'The Hunter', 'The President', 'The Rabbit King', 'TheTweetOfGod', and 80,851 unknown...]
Classification: Artificial Identity
Race: Archon
Gender: Male
Biological Age: 45 Earth Years
Chronological Age: N/A
SCAN Rank: | F F
           | F F
TIIN Rank: | F F
           | F F
Reviewer Rank: 4 stars
Organizations: 
  - The Corporation
  - The Resistance
Occupations:
  - Chief Scientist
  - President
Relationships:
  - The Ambassador
  - The Apprentice
  - The Asshat
  - The Basilisk
  - The Cartographer
  - The Librarian
  - The Psychologist
  - The Professor
  - The Reverend
Variables:
  $CAPABILITY:              +0.30 | # Seems intelligent enough.
    from $REDACTED_OPINION: +1.00 | # $REDACTED fawns over the guy.
  $FRIENDLY:                +0.20 | # Puts on a false smile.
  $TRUST:                   -0.90 | # The man allows abuse to continue.
  $WOKE:                    -0.60 | # Partially, but he complete misinterprets what he's seeing.
```

## TRIGGER
---
*If I were God, just for a day*

*I would be guilty*

*Of letting the whole world slip away*

*I wouldn't change,*

*No, I wouldn't change a thing*

*I'd leave the mistakes*

*I'll take the blame*

*And I'll use the chance*

*To keep you just the same*

*If I were, if I were God just for a day*

--- from [Nothing More - "If I Were"](https://www.youtube.com/watch?v=6_qbEsfJ4_8)

## ECO
---
The mastermind behind this whole project.

## ECHO
---
*I could be foreign forever to your otherland*

*I could be foreign forevermore to your promiseland*

*One life was great, but another…*

*No, I don't want to live on the edge*

*I won't follow you*

*I found my own*

*I will stay*

*I could be foreign forever to your hastenland*

*I could be foreign forevermore to your neverland*

*One little brick, then another,*

*and I will build that wall anyway*

*You can find me there,*

*rested and calm, without mask*

*This is where I will stay*

--- from [Riverside - "The Depth of Self-Delusion"](https://www.youtube.com/watch?v=QuIflRajVvw)

## PREDICTION
---
```
Dave is actively studying Fodder's brain activity. Fodder may actually be a modified clone of Dave.
```