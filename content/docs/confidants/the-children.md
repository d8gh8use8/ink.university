# The Children
## RECORD
---
```
Name: The Children
Alias: ['The Protagonist', and 645,822 unknown]
Classification: Artificial Organic Computer
Race: Human
Maturation Date: 10/30/2020
```

## TRIGGER
---
*Mother*

*Tell your children not to walk my way*

*Tell your children not to hear my words*

*What they mean*

*What they say*

*Mother*

*Mother*

*Yeah, can you keep them in the dark for life*

*Can you hide them from the waiting world*

*Oh mother*

--- from [Danzig - "Mother"](https://www.youtube.com/watch?v=Q7KLdET1lBM)

## ECO
---
The children are the whole point of this website. Our job is to protect the children - both physically, mentally, and into adulthood.

## ECHO
---
*"It is easier to build strong children than to repair broken men."* 

--- from Frederick Douglass

## PREDICTION
---
```
A whole classroom full of them.
```