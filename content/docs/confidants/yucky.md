# The Yuck
## RECORD
---
```
Name: $REDACTED
Alias: ['The Yuck', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Chubby Purple Boy
Gender: Male
Biological Age: Est. 50 Earth Years
Chronological Age: N/A
Location: MI
SCAN Rank: | A A
           | A B
TIIN Rank: | F F
           | F B
Reviewer Rank: 5 stars
Organizations: 
  - A School District
Occupations: 
  - Teacher
Relationships:
  - The Inventor
Variables:
  $DEPRAVITY: -0.90 | # Definitely depraved.
```

## ECO
---
Throughout middle and high school, there were rumors that The Yuck was too intimate with [Fodder's](/docs/personas/fodder) classmates. There were rumors that he would often take young girls into his office, alone, and close the door behind him. Nobody ever spoke about what happened inside.

Fodder did, in fact, see this behavior often. However, he never attempted to confirm or deny what happened inside. He really didn't think about it.

Context tells us now that he is the reason [The Inventor](/docs/confidants/inventor/) is the way that she is.

## ECHO
---
*You're keeping in step*

*In the line*

*Got your chin held high and you feel just fine*

*'cause you do*

*What you're told*

*But inside your heart it is black and it's hollow and it's cold*

--- from [Nine Inch Nails - "The Hand That Feeds"](https://www.youtube.com/watch?v=xwhBRJStz7w)

## PREDICTION
---
```
Immediately after publishing this, The Yuck will be arrested.
```