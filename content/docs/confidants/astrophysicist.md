# The Astrophysicist
## RECORD
---
```
Name: $REDACTED
Alias: ['The Astrophysicist', and 17 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 55 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | A C
           | C D
Reviewer Rank: 3 stars
Organizations: 
  - The Church of Satan
Occupations:
  - Astrophysicist
  - Antiques shop owner
Relationships:
  - The Fodder
  - The Inventor
  - The Raven
Variables:
  $WOKE: +0.20 | # Unsure. It seems likely.
```

## TRIGGER
---
[A visit to her antiques shop.](/posts/journal/2019.12.02.1/)

## ECO
---
The Astrophysicist was the owner of a small antiques store in New York City. With her ties to [The Raven](/docs/confidants/her) and [The Inventor](/docs/confidants/inventor), [Fodder](/docs/confidants/fodder) is sure that she is connected to all of this in some way.

He just isn't sure how.

## ECHO
---
*(Instrumental)*

--- from [Snarky Puppy - "Lingus"](https://www.youtube.com/watch?v=L_XJ_s5IsQc)