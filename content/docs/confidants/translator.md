# The Translator
## RECORD
---
```
Name: $REDACTED
Alias: ['The Translator', and 204 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 40 Earth Years
Chronological Age: N/A
SCAN Rank: | B C
           | A C
TIIN Rank: | B A
           | A C
Reviewer Rank: 4 stars
Location: Dallas, TX
Organizations: 
  - A mental health clinic
Occupations: 
  - Mother
  - Spanish Translator
Relationships:
  - Malcolm Maxwell
Variables:
  $EMPATHY: +0.80 | # She is extremely nice.
  $WOKE:    -0.30 | # It doesn't seem like it.
```

## ECO
---
The Translator is a person that [Malcolm](/docs/personas/fodder) met while in the mental health facility. 

The Translator, herself, was not a patient. She was accompanied by a Hispanic woman who spoke no English. Her story was tragic; apparently, her husband was controlling and abusive, and had very likely been the cause of this patient's mental health problems.

The Translator, however, was a perfectly normal human being. Her and Malcolm got along very well. There was one conversation, in particular, where the two spoke for hours, about all kinds of topics. At one point, she would ask for Malcolm's website - to which he happily obliged.

The next day, when she returned, she didn't look at him. Didn't acknowledge him. And for the rest of the time she was there, she never again spoke to Malcolm.

Clearly, he had scared her.

## ECHO
---
*Bolivia no queda más aire.*

*¿Cuántos quedan?*

*¿Y cuántos son los que se fueron?*

--- from [Hypno5e - "Gehenne Pt. 1, 2, and 3"](https://www.youtube.com/watch?v=u2LD4eBr2Og)