# The Bore
## RECORD
---
```
Name: Molly $REDACTED
Alias: ['GW', 'The Bore', and 621 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 26 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | C C
           | C D
Reviewer Rank: 1 stars
Occupations:
  - Actress
Variables:
  $WOKE:         -0.30 | # Probably not.
  $INTELLECTUAL: -0.70 | # No.
```

## TRIGGER
---
*Pop music 101*

*Some simple instructions, for a good first impression*

*Now lets start with verse one*

*A minor chord, tensions grow*

*Fade in the bass like so*

*Now with momentum go*

*Stop*

*And bring the beat back*

--- from [Marianas Trench - "Pop 101"](https://www.youtube.com/watch?v=yBDNvlvR8vA)

## ECO
---
The Bore is the kind of woman that brings [Fodder](/docs/personas/fodder) to tears. She talks about her trip to the grocery store. And the long line. She talks about taking her dog to daycare. For 10 minutes. She talks about her friend, Christine. Gossip. 

The monotony of her interests gives Fodder anxiety. He finds her so completely uninteresting, he'd rather gouge-out his ears than listen to what she has to say.

## ECHO
---
*We are dirt*

*We are alone*

*You know we're far from sober?*

*We are fake*

*We are afraid*

*You know it's far from over*

*We are dirt*

*We are alone*

*You know we're far from sober?*

*Look closer*

*Are you like me?*

*Are you ugly?*

--- from [The Exies - "Ugly"](https://www.youtube.com/watch?v=4QS-mKQWOZI)