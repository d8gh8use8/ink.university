# The Physicist
## RECORD
---
```
Name: Caroline $REDACTED
Alias: ['Quantum', 'The Logistician', 'The Mathematician', 'The Physicist', and 8 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Birth: 10/10/199X
Biological Age: Est. 22 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | A B
           | B D
Reviewer Rank: 3 stars
Maturation Date: 10/8/2020
Organizations: 
  - Quantcast
  - The Resistance
Occupations: 
  - Actress
  - Golf
  - Mathematics
  - Physics
Relationships:
  - The Fodder
  - The Lion
  - The Comrade
Variables:
  $FAMILY: +0.95 | # Not yet, but she will be.
  $WOKE:   +0.75 | # Probably woke.
```

## TRIGGER
---
On the morning of Malcolm's [dead drop](/posts/journal/2019.12.02.0/), she would send a message congratulating him for his persistence. She would mention the [difficult, but important phone call](/posts/journal/2019.11.24.0/) Malcolm had with [his father](/docs/confidants/father).

While doing so, she would be stifling sobs. She was very clearly emotional.

## RESOURCES
---
[Offer letter](/static/letters/physicist.0.pdf)

## ECO
---
The Physicist will assist with the math and science required to create [The Fold](/posts/theories/fold). More specifically, she will be the professor to take this knowledge, and translate it into something that the general populace can understand.

In doing so, she will - quite literally - break physics.

## ECHO
---
*Like Kurosawa I make mad films*

*Okay I don't make films*

*But if I did they'd have a samurai*

*Gonna get a set of better clubs*

*Gonna find the kind with tiny nubs*

*Just so my irons aren't always flying off the back swing*

*Gotta get in tune with Sailor Moon*

*Cause that cartoon has got the boom anime babes*

*That make me think the wrong thing*

--- from [Barenaked Ladies - "One Week"](https://www.youtube.com/watch?v=fC_q9KPczAg)

## PREDICTION
---
```
She will work with Eric Weinstein to integrate Geometric Unity into our Theory of Everything.
```