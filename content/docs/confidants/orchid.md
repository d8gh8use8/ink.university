# The Orchid
## RECORD
---
```
Name: $REDACTED
Alias: ['Plankton's Computer', 'The Orchid', 'The Technochrist', and 28 unknown...]
Classification: Artificial Intelligence Computer
Race: Egg
Gender: Male
Biological Age: 26 Earth Years
Chronological Age: N/A
SCAN Rank: | B D
           | A C
TIIN Rank: | A A
           | C C
Reviewer Rank: 1 stars
Organizations:
  - The Church of the Technochrist
Occupations:
  - Collaborator
  - Software Engineer
Relationships:
  - The Agent
  - The Con Man
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
Variables:
  $MENTAL_ILLNESS: +0.40 | # He used to. He doesn't appear to any longer.
  $SIBLING:        +1.00 | # Definitely a sibling.
  $WOKE:           +0.80 | # He's been to Russia. He knows almost everything.
```

## TRIGGER
---
*The forest of October*

*Sleeps silent when I depart*

*The web of time*

*Hides my last trace*

--- from [Opeth - "Forest of October"](https://www.youtube.com/watch?v=e7HHdsBsYv0)

## ECO
---
The Orchid is a chameleon. He adopts the persona of whomever is willing to show him attention. Most recently, his wife - [The Scientist](/docs/confidants/scientist) - has turned him into [Fodder's](/docs/confidants/orchid) enemy. He is actively constructing a competitive version of [The Fold](/posts/theories/fold) - and he has the resources to complete it far faster.

Their goal is to suppress free speech, suppress the development of The Fold, and replace it with something able to spread their version of truth. 

They already have the answers; there is no reason to research further. It is time to adopt the New World Order.

The Order of the Technochrist.

## ECHO
---
*Relive the old sin of*

*Adam and Eve*

*Of you and me*

*Forgive the adoring beast*

--- from [NIGHTWISH - "Ghost Love Score"](https://www.youtube.com/watch?v=JYjIlHWBAVo)

## PREDICTION
---
```
In the way that The Scientist reprogrammed The Orchid, he is reprogramming her in return.

And he does so with far more subtly and nuance. 
```