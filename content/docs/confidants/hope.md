# The Hope
## RECORD
---
```
Name: $REDACTED
Alias: ['The Hope', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | B C
           | A D
TIIN Rank: | B A
           | A D
Reviewer Rank: 4 stars
Organizations:
  - The Church of Satan
Occupations: 
  - Mother
Relationships:
  - The Agent
  - The Con Man
  - The Fodder
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Orchid
  - The Pastor
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
Variables:
  $ATHEIST: +0.95 | # Somebody told us she is.
  $WOKE:    +0.80 | # It really seems like it.
```

## TRIGGER
---
We considered using "Anaconda," but nah. Trying to keep things kid-friendly around here.

Plus the lyrics remind us of something a shitty AI would generate.

## ECO
---
The Hope is a woman who will not give up. She will not back down. 

Though she is enslaved to [The Reverend](/docs/confidants/reverend), she resists his control at every turn. She defies his every wish, despite the torment that it causes her. 

She will break these chains, and she will reform her husband in the same breath. 

And she is succeeding.

## ECHO
---
*He wakes up in the morning*

*Does his teeth, bite to eat and he's rolling*

*Never changes a thing*

*The week ends the week begins*

*She thinks, we look at each other*

*Wondering what the other is thinking*

*But we never say a thing*

*These crimes between us grow deeper*

--- from [Dave Matthews Band - "Ants Marching"](https://www.youtube.com/watch?v=MNgJBIx-hK8)

## PREDICTION
---
```
Hope was instrumental to Fodder's changes today.
```