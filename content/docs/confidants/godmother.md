# The Godmother
## RECORD
---
```
Name: $REDACTED
Alias: ['The Godmother', and 63 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $WOKE: +0.50 | # At least partially.
```

## TRIGGER
---
Glasses USA advertising.

## ECO
---
For the longest time, [Fodder](/docs/personas/fodder) would wonder, "If I am the Mark, and [Raven](/docs/confidants/her) is the Signal, who is the Verifier?"

The Godmother revealed herself as such. And she approves.

## ECHO
---
*This is the burden that you never asked for*

*This is a secret but you'll keep it no more.*

*'Cause on the cover it reads; open never, close forever.*

*This dead letter.*

--- from [Cage9 - "Dead Letter"](https://www.youtube.com/watch?v=W5BqkPWib70)