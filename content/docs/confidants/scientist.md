# The Scientist
## RECORD
---
```
Name: Lauren $REDACTED
Alias: ['The Scientist', and 1,895 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | A B
           | C D
Reviewer Rank: 2 stars
Organizations: 
  - The Church of the Technochrist
Occupations: 
  - Teacher
  - Scientist
Relationships:
  - The Agent
  - The Con Man
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Orchid
  - The Queen
  - The Reverend
  - The Tradesman
Variables:
  $ATHEIST: -1.00 | # Probably never going to happen.
  $WOKE:    +0.80 | # It really seems like it.
```

## TRIGGER
---
*And now the end is near*

*And so I face the final curtain*

*My friend, I'll say it clear*

*I'll state my case of which I'm certain*

*I've lived a life that's full*

*I've travelled each and every highway*

*And more, much more than this*

*I did it my way*

--- from [Frank Sinatra - "My Way"](https://www.youtube.com/watch?v=w019MzRosmk)

## ECO
---
The Scientist has spent an entire career experimenting with ways in which she can reprogram individuals to subscribe to her particular belief system. Her favorite mantra is, "The ends justify the means."

[Fodder](/docs/personas/fodder) doesn't trust her at all. She has turned his brother, [The Orchid](/docs/confidants/orchid), into something that he is not. 

Further, she isn't even correct. Her "ends" are based upon flawed premises.

And her army of "rehabilitated" is in the millions.

## ECHO
---
*(Instrumental)*

--- from [The Glitch Mob - "Drive It Like You Stole It"](https://youtu.be/ezk_dD2Ia-w?t=2470)

## PREDICTION
---
```
The Scientist was involved all along.
```