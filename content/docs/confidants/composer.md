# The Composer
## RECORD
---
```
Name: Alana $REDACTED
Alias: ['The Composer', 'The Supermodel', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 28 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C F
TIIN Rank: | C C
           | B F
Reviewer Rank: 1 stars
Occupations: 
  - Actress
  - Audio composition
Variables:
  $PEACE: +0.70 | # She exudes peace.
  $WOKE:  +0.20 | # Maybe.
```

## ECO
---
This woman is able to orchestrate sound in real-time to achieve tranquility and relaxation within the beholder, as well as herself.

She is extremely skilled at audio composition.

## PREDICTION
---
```
She will create the soundscapes used within the first ASMR-themed businesses.
```