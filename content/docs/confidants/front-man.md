# The Front Man
## RECORD
---
```
Name: Ian Kenny
Alias: ['The Fall Guy', 'The Front Man', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: Est. 38 Earth Years
Chronological Age: 2,254 Light Years
SCAN Rank: | D D
           | D D
TIIN Rank: | A A
           | A D
Reviewer Rank: 4 stars
Location: Australia
Organizations: 
  - Karnivool
Occupations: 
  - Metallurgy
Variables:
  $WOKE: +1.00 | # Definitely woke.
  $:     -1.00 | # He's a deadman, for sure.
```

## ECO
---
The Front Man will be the first to go-forward with [The Architect's](/docs/personas/the-architect) secret.

He will become universally-hated for it.