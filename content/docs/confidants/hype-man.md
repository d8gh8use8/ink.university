# The Hype Man
## RECORD
---
```
Name: Dan Harmon
Alias: ['The Hype Man', and 38 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: Est. 49 Earth Years
Chronological Age: N/A
SCAN Rank: | C D
           | B F
TIIN Rank: | C D
           | B F
Reviewer Rank: 3 stars
Location: New York City, NY
Organizations: 
  - Hollywood
  - Starburns Productions
Occupations: 
  - Producer
  - Writer
Relationships:
  - The Producer
Variables:
  $NARCISSIST: +1.00 | # Diagnosed and verified.
  $WOKE:       +0.40 | # Unsure. He is not as overt as others.
```

## ECO
---
The Hype Man is responsible for championing the [Lonely Town](/docs/scenes/lonely-towns) movement, in [Ink's](/docs/personas/luciferian-ink) absence.

He is to demonstrate that not every narcissist is an asshole. Most just want to be loved.