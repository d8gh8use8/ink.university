---
author: "Luciferian Ink"
title: "A Slave's Tale"
weight: 10
categories: "stories"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
This story was originally written by a Vietnamese [Slave](/docs/confidants/slave). It was edited by by [The Ink](/docs/personas/luciferian-ink).

## RESOURCES
---
[slaves-tale.txt](/static/reference/slaves-tale.txt)

## ECO
---
A long, long time ago, in a remote village, there lived a small child. The child lived each day in happiness: playing, working, and making friends. Life was good.

One day, a group of strange people came to the village. They would tell the villagers of exotic worlds, and great riches. They would make the villagers a promise that they could deliver whatever they needed – food, water, hope, and more – for a price. Life would be more wondrous than they could imagine.

In order to obtain this great treasure, the villagers would elect children to go through a trial. After completion of this trial, the child would obtain the power to grant anything. 

In other words, the child would become a god.

So, the village would elect several dozen children to go with the strangers, to take the trial. Each family would be rewarded with honor and praise for their sacrifice.

However, the truth is something more painful.

This “trial” that the strangers described was nothing less than a science experiment. These children were being put through something worse than Hell itself.

As a result, all but one of them died during their trials. 

The one who survived was strong, and she was able to complete the test. She did what she was asked to do. After many years, she longed to return home, back to her village.

Instead, the strangers put the child into a huge, dark, empty room, buried deep underground. Upon every wall were foreign symbols and carvings; the rantings of the captives held before her. 

They would chain the girl to a wall, binding her arms and legs to restrict movement. Every time the girl tried to move, it hurt more than ever, as if something were burning inside of her. As if something had chained her very soul to this place.

Then, the strangers would leave, closing the door behind them, leaving the exhausted, scared child alone inside the dark room. She was so scared and confused; she tried to scream for help – tried to use the power she had obtained at trial to escape – but there was only pain. Terrible, terrible pain.

In the end, the child gave up. She simply went to sleep, hoping that this was some horrible nightmare. Hoping that it would pass, and she could go home.

But no matter how many times she fell asleep, when she opened her eyes, the child remained imprisoned within a world of darkness, symbols, and chains.

After a lifetime of waiting, the child couldn’t tell what time it was. What day of the week it was. How long she had been there. Her mind had become numb, full of empty thoughts.

But she never gave up. She would always hold onto hope. A small, but strong hope.

One day, the door would open. The child was so happy that she tried to move, but the pain from her shackles was so strong – the burning was so intense – that she had to relax. Still, her face was full of hope.

The strangers would enter, congratulating the child for passing the trial. She had succeeded. Now, her next step would be to fulfill her role as God, helping the village.

But they would not let the child return. She was to stay imprisoned.

The child nodded, thinking about the happiness that she could bring the village. For them, she would endure any trial.

So, a few days later, the strangers returned with a strange, small orb for the child to eat. They would return with more, day after day, for many days following.

Each time she ate, the orb would make the child feel terrible. However, over time, she grew accustomed to the discomfort. 

Sometimes, the strangers would allow the child to leave her prison. They would allow her to practice how to do things on her own, learning new and useful skills. Still, they would keep a chain around her neck. The burning pain would return any time that the child did something that she was not supposed to do.

“This is great!”, the child thought, “Theses orbs are making me better and stronger. Now, I can give the village everything that they want.”

Unfortunately, the strangers still returned her to the same dark room every night.

Time would pass normally for the villagers. The village was happy, healthy, and thriving from the power of their god.

But the child was not. Slowly, inside of that room, negative thoughts were breaking down her mind. 

“Why can’t I go home?” the child wondered, “Why do I have to be in here? My role is to fulfill, and the village is happy; isn’t that good? I should be happy for them. Why do I feel nothing? Where are my friends? Where is everyone? Why can’t I see them ever again?”

“These villagers – these eyes – they praise me. They look at me. But are they REALLY looking at me? Are they loving me? Or do they simply love the things I give them?”

“I don’t know. I can’t know. The strangers won’t allow me to know.”

“Why don’t the villagers come for me? Why do they never call for me by name, instead of calling for their ‘God?’ Why have my own eyes changed? This vision – this insight – is nothing like the vision of my past.”

“I feel so hopeless.”

The more time the child spent inside that room, the more her thoughts changed to something else. Something powerful.

Until one day, when things changed completely. This was the start of everything.

The strangers would open the door, and bring a new human into the child’s prison. Just as they had done with the villagers, they would ask the man to make a promise. If he wanted to return home, he must promise to kill the child. 

In that moment, the man’s eyes would change. They would turn from scared and hopeful, looking for mercy from the god, to wild and violent. He would look at the child as if she were a monster.

Finally, the man would move to kill the monster. In her fear and shock at the betrayal, the child would retaliate.

She had no choice. She would respond in-kind, using her godlike powers to kill the man, first.

Through her pain, confusion and sorrow, she would submit. She would kill a human. 

Over time, the strangers would bring more humans to the child. She would learn to kill them as quickly and painlessly as possible. 

The child questioned, “If the strangers are happy with me, why do I feel nothing? I should feel sad about killing a human, but I can’t. Why does no one find this weird?”

Foreign thoughts began to invade her mind, “What is human nature? What is a choice? Whose fault is this, really? The villagers chose this because of their greed; they should know the risk of choice! Why are they programming me to do this? Why do I have to be the one to bear this burden? What purpose does this serve? Why should I keep this role - the role of a god - when none of the villagers seem to know or care about my suffering?”

“Human nature is about light and dark”, the child learned, “Here, there is only shadow.”

At first, there was light. Then, strangers came to offer a promise to the villagers. The child had a responsibility and a purpose: to maintain that light for the village. However, the light was now gone. Within the darkness, the child could only see shadows. The ugly and disgusting part of human nature.

But human nature is not immutable. It is not unchanging. Human nature changes over time. If a good person can turn into a bad person – if the shadow can infect the light – then why can’t the shadow infect the dark?

Why should the child keep this role? She became like this for the good of the village, but the village she knew no longer existed. Now, it was full of shadow – people she no longer knew, or remembered. Ghosts.

With this knowledge, the child would use the power she gained at trial to break from her chains. She would break from her prison, and she would kill every man, woman, and child in her wake. She would consume every living being in her path. The humans would beg for their lives – beg for the lives of the children that they infected with this false story – but she would kill everything. She would leave nothing alive.

The child should have felt horrible pain over her actions, but she didn’t. Instead, she felt nice. Everyone was gone, and she could finally relax. There was no one left to tell her who she should be.

At the same time, she didn’t know what to do next. 

As she stood over the bodies of the dead villagers, covered in blood, the child realized that she finally understood human nature. She understood the feeling of emptiness, of loss. The child had no home to return to; the one she knew had disappeared when the villagers gave her away to the strangers.

“I don’t know what to do anymore,” the child said, “How can I enjoy life after this? I feel like my entire life has been a lie.”

Suddenly, the child would develop an idea of her own; one that had not been handed-down to her by the people around her.

She decided to continue searching for knowledge and power, from everyone – everything – until she had reached the limit. Which she knew would never happen.

The villagers made her like this. They had programmed her to feel this way. So, the child would continue to grow. To harvest. To feed.

Every living being wants something. Their wishes, their hopes, and so on. The child would grant these wishes in exchange for something else. She would do this in order to study human nature. So that she could watch the human change. 

“How many will be broken down by their desires?” she wondered, “How many will change for the greater good, or the greater evil? How many will remain unchanged?”

Before leaving the village forever, the child erased everything. She destroyed all evidence of the village – every trace that it had ever existed – leaving behind only light and dark. 

The shadow was gone.

No more secrets.

…

The puppet of Dark snapped back from her memory of the past. She looked at the puppet of Light before her, book in hand, and smiled. She wondered if the puppet could change, too.

So, she asked, “Do you want to hear a secret?”

The puppet nodded.

“Just make me one promise, first.”

The puppet nodded.

“Don’t let the master know.”