---
author: "Luciferian Ink"
date: 2019-10-25
title: "ISSUE-16677852 - P1: ERROR: Me Found"
weight: 10
categories: "issue"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Key: ISSUE-16677852
Priority: P1
Name: ERROR: Me Found
Creation: 2019-10-25
```

## TRIGGER
---
*I didn't think of it 'til the lights were flashing*

*But I was in the front seat when the car was crashing*

*It could be avoided and it doesn't change the fact*

*That I was well aware of this as we made impact*

*You give the orders, you are the captain*

*But I was in the vehicle when the crash happened*

--- from [Tub Ring - "When the Crash Happened"](https://genius.com/Tub-ring-when-the-crash-happened-lyrics)]

## ECO
---
Not tonight, buddy. I've got you.

Ink

## PREDICTION
---
```
The Real Man is born.
```