---
author: "Luciferian Ink"
date: 2007-01-24
title: "Equal Pay for Equal Work"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---
It’s as easy as that. Why should any woman be discriminated in the workplace, when most often they can complete the same job as any man, just as well, sometimes better? Pay needs to be based entirely on skill; basing it on anything else is not only unfair, but it is wrong.

I can see being more discriminatory in certain fields of work as being a necessary evil. A woman’s body is not built as a man’s, therefore, how could one expect a woman to be able to work certain physical jobs, and receive equal pay for lesser work? Every year in high school, our football team would play against one with a girl on it. Nobody wanted to have to go against her, for fear of hurting her, yet it wasn’t like she was small. In fact, she was considerably bigger than most men on the team. She simply did not have the build of a man, and didn’t have the strength to compete. I am not basing this on just one person; there were others of which followed the same trend. One even played on my team for a while. This is not to say that ALL women would be unfit for a job like this; and if a woman can do the work just as well, then by all means, she deserves the opportunity. Women now make up about 9 percent of the people working in the construction industry today, with approximately 1,131,000 total. That is an increase in 4.82% from 2005 to 2006. (“Facts” 1) Obviously, they are doing something right. 

Some would argue that men have proven again and again to be great leaders, but it seems that women have not been given much of a chance. They often become stuck at a sort of glass ceiling, or a situation in which the advancement of a qualified individual within the hierarchy of an organization is stopped by discrimination, be it sexism, racism, or others. (“Anderson” 467) Dawn Rosenberg Mckay, a career planning professional with over a decade’s worth of experience and co-owner of Kangaroo Research Mavens, a researching company to businesses and individuals, tells her story of reaching the glass ceiling. After graduating college, she discovered that employers were more interested in whether or not she could type, rather than the type of degree she had. While taking a typing test after applying at a specific firm, she noticed a friend with a similar degree to hers, a male, walk in, and planned to say hi after he came into the room to test with her. 

‘He never did… Next thing I knew she was on the phone with the personnel (now called human resources) office of one of the agency's clients… "We have a great candidate for you… I think you should interview him for that marketing assistant position…" "Hey, wait a minute," I thought. "What about his typing test?" ‘ (Mckay)

In this case, not only has Dawn reached a glass ceiling, in which the stereotypes of women being secretaries have kept her from an equal opportunity at a job, but her friend has encountered the glass escalator. This defines the rapid promotion of men over women, even in a woman dominated field such as nursing, to a managerial position. (“Glass”) These stereotypes are what keep women down, and are what must be broken if women are ever to advance. But how are they to advance when these stereotypes keep them down. It’s a catch twenty-two not unlike that of which African Americans found themselves in after being released from slavery. 

Needing money for education and such, they sought work, but soon found that discrimination kept them from getting the work they needed. Thus, they remained in poverty, while the white men blamed them for their troubles. It is no wonder that affirmative action laws became a necessity in the 1960’s. Even with these, minorities, too, can reach a glass ceiling.

It’s a situation that will need to be resolved, first, by breaking the hold white men have over women and minorities. Women and minorities are alike in the way that both are capable of any job, therefore should be considered every bit as much as a man, and paid as such. After all, America is a land of equality, right?

### Works Cited

Anderson, Margaret L. and Howard F. Taylor. Sociology: The Essentials. 4th ed. Belmont: CA 2007.

“Facts” NAWIC. National Association of Women in Construction. Oct. 2007 <http://www.nawic.org>

“Glass Ceiling” Reference.com: Encyclopedia. Reference.com. 2008. <http://www.reference.com>

Mckay, Dawn Rosenberg. “Women Face Glass Ceiling in Hiring.” About.com: Career Planning. Dawn Rosenberg Mckay. About.com. 2008. < http://careerplanning.about.com>
