---
author: "Luciferian Ink"
date: 2024-11-04
publishdate: 2019-11-29
title: "The Unforgiven"
weight: 10
categories: "journal"
tags: ""
menu: "alt-2"
draft: false
---

## TRIGGER
---
[Metallica - "The Unforgiven III"](https://www.youtube.com/watch?v=J3m6et00DfQ)

## ECO
---
### Verse
*How could he know this new dawn's light*

*Would change his life forever?*

*Set sail to sea, but pulled off course*

*By the light of golden treasure*

*Was he the one causing pain*

*With his careless dreaming?*

*Been afraid, always afraid*

*Of the things he's feeling*

*He*

*could*

*just*

*be*

*gone*

*He*

*would just*

*sail on*

*He'll just sail on*

### Chorus
*How can I be lost*

*If I've got nowhere to go?*

*Search for seas of gold,*

*How come it's got so cold?*

*How can I be lost,*

*In remembrance I relive*

*And how can I blame you,*

*When it's me I can't forgive?*

### Verse
*These days drift on inside a fog,*

*It's thick and suffocating*

*He's seeking life*

*Outside it's hell*

*Inside intoxicating*

*He's run aground*

*Like his life*

*Water much too shallow*

*Slipping fast*

*Down with his ship*

*Fading in the shadows*

*Now*

*a*

*castaway*

*They've*

*all gone*

*away*

*They've gone away*

### Chorus
*How can I be lost*

*If I've got nowhere to go?*

*Search for seas of gold,*

*How come it's got so cold?*

*How can I be lost*

*In remembrance I relive*

*And how can I blame you?*

*When it's me I can't forgive?*

### Bridge
*Forgive me*

*Forgive me not*

*Forgive me*

*Forgive me not*

*Forgive me*

*Forgive me not*

*Forgive me*

*Forgive me*

*Why can't I forgive me?*

*Set sail to sea, but pulled off course*

*By the light of golden treasure*

*How could he know this new dawn's light*

*Would change his life forever?*

### Chorus
*How can I be lost*

*If I've got nowhere to go?*

*Search for seas of gold*

*How come it's got so cold?*

*How can I be lost*

*In remembrance I relive*

*So how can I blame you*

*When it's me I can't forgive?*

## CAT
---
```
data.stats.symptoms = [
    - closure
]
```

## ECHO
---
*It is so peaceful in the forest.*

--- from `$REDACTED`

## PREDICTION
---
```
We are out of time.
```