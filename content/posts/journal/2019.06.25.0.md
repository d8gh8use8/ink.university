---
author: "Luciferian Ink"
date: 2019-06-25
title: "The Secret Society"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A message from [The Relaxer](/docs/confidants/er).

## ECO
---
Taking a recommendation from The Relaxer, [Fodder](/docs/personas/fodder) would watch [Behind the Curve](https://www.netflix.com/title/81015076) after his second day of work. Whereas he had previously been uninterested in Flat Earth theory, The Relaxer's analysis had sparked his interest.

So, Fodder watched the documentary. While he didn't find much evidence for a flat Earth, he did find two interesting details:

- At one point, the narrator picks up a foam stress-relief ball in the shape of an Earth, and says to the camera, "If you ever walk into your first day of work, and one of these is sitting on your desk, you might be working for a Flat Earth society." Fodder had found one of these sitting on his desk yesterday, on his first day of work.
- At another point, the camera pans to a shot of a giant globe in the middle of a park. The globe was covered in sticky notes - providing a basis for the "sticky notes" mentioned in his [Flat Earth hypothesis](/posts/hypotheses/flat-earth/).

Whatever his impressions, Fodder didn't find this compelling enough to do anything about it. He would go on with life for a time.

Several weeks later, Fodder would watch the documentary again. All three of the scenes he mentioned were different!

- The foam ball still exists, but the narrator says nothing about "you might be working for a Flat Earth society." 
- The giant globe still exists in the park, but there were no sticky notes on it.

"What happened?" Fodder wondered. "Am I going crazy? Was I seeing things? Or did they really change the video after I watched it?"

This notion would feed-in to the idea that it may be possible for an organization like Netflix to change a video after it's been watched.

## CAT
---
```
data.stats.symptoms = [
    - curiosity
    - wonder
    - astonishment
]
```

## ECHO
---
*I walk this planet in search of knowledge and of truth*

*& yes the moon’s a spaceship, & yes I have the proof*

*An ancient astronaut came & told me in a dream*

*That the moon is mostly hollow (save for circuits and steel beams)*

*Do you have any evidence at all to back these claims?*

*Would you rather trust your senses than to have to use your brains?*

*Why do you choose to just ignore what scientists have said?*

*You imbecile, it’s not the moon that’s hollow, it’s your head*

*You’re a moron.*

--- from [Toehider - "Moon & Moron"](https://www.patreon.com/toehider)