---
author: "Luciferian Ink"
date: 2019-07-05
title: "Black Market"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Some people seem to think*

*They always know what's best for you*

*Their little minds try to*

*Create a world to keep you still*

*The bolt is thrown, the cage is locked*

*You saw this, don't you lie*

*At first you cry and then you hate*

*Those people stole your will*

--- from [Ra - "Do You Call My Name?"](https://www.youtube.com/watch?v=lrfP67jNB2U)

## ECO
---
### The Delivery
[Fodder](/docs/personas/fodder) would purchase marijuana from the dark web three times over his lifetime. After careful research, he had come to the conclusion that this was safer than driving to Colorado for it, as he had also done three times in the past. His life was so miserable that he was willing to take any risk just to obtain some peace-of-mind.

This time, he would purchase from a new marketplace. His old one had mysteriously disappeared, and he had to improvise. He needed to take a risk on a new marketplace.

He was pleased to find that his favorite vendor, [The Resistance](/docs/candidates/the-resistance), was also on the new marketplace. He purchased some product, and waited for delivery.

Which was today. Fodder received his package in the mail just after work.

He was horrified to find that he had only received half of his order. The other half was missing. Had it been seized by the post office?

The thought terrified Fodder. He would spend the evening hiding all of his paraphernalia, cleaning the house, and occasionally peaking through the blindfolds in his front window.

He was sure that they were coming.

### The Private Investigator
Over the next few days, Fodder would check his mailbox several times, hoping to find the second package. It never arrived.

However, he would notice a white Jeep parked just down the street. He had never noticed it before - and he certainly should have, because it was right next to the mailbox. It seemed to never move, and it had tinted windows - such that Fodder couldn't see inside of it at all.

He was sure that he was being watched. Despite that, he would continue to check his mailbox, acting as if nothing was wrong. As if he hadn't just received drugs in the mail.

After a week of waiting (and staying clean), Fodder would decide to log into his marketplace account, just to see if anything had happened.

Indeed, he had received a message from the vendor, telling him that he had run out of product, and refunded his second order.

Fodder breathed an sigh of relief. All of this worrying was for nothing. He was fine.

But he could not take that money back. The money, and the account were as good as dead. If he were to retrieve his bitcoin, the authorities would be able to trace the transaction back to his accounts. 

A couple hundred dollars lost for his protection? He would take the hit.

### The Neighbor
With his newly-found freedom, Fodder would break into his new product. Relaxing into his lounge chair, he would turn to Netflix, before being abruptly interrupted by a knock at the front door.

"Shit," Fodder remarked. He was high. There was no way he was going to try and answer the door.

But the person persisted. They would not go away. After several minutes of knocking, Fodder would make his way to the front door.

Opening it, he would find his neighbor. The one that in two year's time, he had never interacted with before.

"Hi!" she said, "I'm your neighbor."

"Can I help you?" Fodder replied, sheepishly, stepping outside, and closing the door behind him. He knew that his eyes must be bright red, "Sorry if you've been waiting. I had headphones on."

"No problem," she said, "I'm about to put my house on the market. I was wondering if you could fix the broken part of the fence, on your side?"

"Oh! Not a problem. I can do that tomorrow."

"Thank you so much," she continued. 

It was then that Fodder recognized the look in her eyes. Bright red, watery - she was high, too. She was smiling knowingly.

The two continued their pleasantries, before she returned to her home, and Fodder returned to his.

"Well, fuck," Fodder said. "Not only did they cancel my order, trying to trick me into retrieving the bitcoin that they would use to prosecute me, but they sent the second package to my neighbor!"

This was a warning:

*"We are watching. We know what you're doing. You won't get a fourth chance."*

The white Jeep was gone the very next day.

## CAT
---
```
data.stats.symptoms [
    - misery
    - desperation
    - paranoia
    - fear
]
```

## ECHO
---
*Moon light burns my face as I awake, where I did not intend to sleep*

*The field soaked with blood, they smell my flesh*

*The crowds calling me*

*As I arise, and run from this, I cannot help but to collapse*

*The calling*

*They shelter me*

*I'm coming down and down again*

*Down*

*Moon light burns my face as I awake, where I did not intend to sleep*

--- from [Trenches - "Calling"](https://www.youtube.com/watch?v=UKB9dJTF3lw)