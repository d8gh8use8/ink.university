---
author: "Luciferian Ink"
date: 2019-12-30
title: "The Escape"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A message from [The Investigator](/docs/confidants/investigator).

## ECO
---

### Search
[Malcolm](/docs/personas/fodder) was awoken in the early morning hours by the sound of loud voices, and banging doors. From the main room, he could hear several police officers speaking to the medical staff.

"Where is he?" one of the officers asked.

"Sleeping, in room number 5," the technician responded.

The police proceeded to enter a different room - the storage room - to search everyone's belongings. They would take a particular interest in Malcolm's cell phone.

Just then, a technician and a man in a lab coat entered his bunk.

"That's Malcolm, right there," said the technician, pointing. 

"Wha...?" Malcolm said, sitting up.

"Malcolm, this is Brian. You can go back to sleep."

He laid back down again. The two whispered briefly, before turning to leave. As they walked out the door, the man in the lab coat muttered:

"I've got your back, son."

"Dad?" Malcolm wondered.

### Extraction
After an hour or two of searching, the police would leave. Clearly, they didn't find what they had come for.

Just then, the technician entered Malcolm's room once more.

"Okay Malcolm," he said, "It's time to go."

Malcolm sat up.

The two would quietly exit the facility.

...

Standing out front, Malcolm was greeted by his former co-worker, `$REDACTED`. He smiled brightly, and said, "Well, I guess you passed!"

"I guess so," Malcolm responded, wearily. 

"Come on, let's get you out of here."

The two climbed into a red truck.

"Put this on," he said to Malcolm, handing him a hood. Malcolm complied, putting it over his head.

`$REDACTED` started the truck, and they were off.

### Answers
Malcolm would ask a few questions, but `$REDACTED` wouldn't answer, saying, "They told me not to talk with you."

After about 30 minutes, the two would arrive at their destination. `$REDACTED` would remove the hood.

"Alright, well good luck out there. You're going to need it."

"Thanks," Malcolm replied, exiting the vehicle, "for believing in me."

`$REDACTED` nodded.

Malcolm stood in the center of an empty parking lot. Before him was a semi-truck, with the back opened. He was approached by a man that he immediately recognized:

"`$REDACTED`!" Malcolm exclaimed. "You came through for me after all!" he said, [recalling the gift he had left with the man](/posts/journal/2019.12.10.0/).

"Good fucking job, kid," `$REDACTED` replied. "Climb in. We're going to get you out of here."

"What's going on? Where am I going?"

"It's better if you don't know," he replied. "Just in case you get caught."

"Hmm," Malcolm replied.

"Come on. We don't have much time."

"Well, I've made it this far. Not going to give up now."

"There we go," he said, smiling. "Get in. We'll talk soon."

With that, Malcolm climbed into the bed of the truck. He entered a large crate labeled "LIVE ANIMAL," and laid down upon the mattress within. Immediately after, the crate was nailed-shut from the outside.

Malcolm's heart was racing.

### Thought
Malcolm would wait quietly as he was transported to the airport. He would listen as the flight technicians worked around him, eventually loading the crate into the airplane. He would wait until the plane had taken-off before turning on the small lantern they provided him with.

The box was claustrophobic, triggering Malcolm's anxiety. While he still wasn't sure if he could trust the people transporting him, he was comforted by the steps they had taken to support him.

In the corner was a small, portable toilet. Next to it was a backpack containing food, beverages, a book, and several other items. His bed was soft, and the blanket was warm.

Malcolm's mind was racing. In his head, he was playing-out all of the possible outcomes of this scenario. Many of them were negative. Despite that, he attempted to lay down, and get some sleep.

After a couple of hours, he still couldn't relax. So, he would again turn on the light, pulling-out the book he had been provided with. "Beyond Good and Evil," by Friedrich Nietzche. 

As he did so, a polaroid dropped from the pages, falling to the floor. Curious, Malcolm would turn it over. He gasped.

It was a picture of [The Raven](/docs/confidants/her), smiling brightly. She held a painting before her - no doubt [the one she had specifically-made for Malcolm](/posts/journal/2019.11.26.2). In it was the picture of a man engulfed in blue flames, floating high above a lake in the pale moonlight. It was exactly what Malcolm had predicted she would make for him. It was exactly what he had hoped to find in this crate.

He began to cry.

### Arrival
`$REDACTED`

## CAT
---
```
data.stats.symptoms = [
    - wonder
    - fear
    - anxiety
    - anticipation
]
```

## ECHO
---
*Breathe this in and let this dose be of reality*

*To the world as you were truly meant to perceive it*

*Infinite shades of reasons and truth*

*Of fulfillment and identity, of vice and virtues*

*Let the curtains close across the stage you've known*

*And the spotlight fade for the chance to see*

*What resonates within your very core*

*Will light the stage that you were meant to perform*

--- from [Intervals - "The Escape"](https://www.youtube.com/watch?v=Var6j5gD2qM)