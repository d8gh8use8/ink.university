---
author: "Luciferian Ink"
date: 2024-11-01
publishdate: 2019-11-23
title: "I Have Little to No Memory of These Memories"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Toehider - "59 Songs You MUST Hear Before You Die!"](https://www.patreon.com/toehider)

- [49 songs playlist](https://soundcloud.com/mike-toehider/sets/49-songs-you-must-hear-1/s-pGiOo)
- [10 covers playlist](https://soundcloud.com/mike-toehider/sets/10-albums/s-Eeg5I1kH2xv)

## ECO
---
I awoke this morning with one of the worst headaches I've ever had in my life. I don't remember drinking last night. Then again, I don't remember anything at all. Strange.

The right side of my neck was on fire. Stepping into the mirror, I saw the Mark of the Beast branded into my neck: 

[![The Mark of the Beast](/static/images/singularity.grey.png)](/static/images/singularity.grey.png)

What in the world? I don't remember getting a tattoo!

Regardless of the confusion, I left my suite, crossed the grounds, and walked through the atrium into the [Sanctuary of Humanity](/docs/scenes/sanctuary) for my morning meditation - just as I always do. Finishing that, I prepared for the day - bathing and eating - and returning to my room to write. 

Sitting at my desk, I powered-up the computer, turning to look out of the floor-to-ceiling windows. The view was stunning, as always. Great beams of light filtered into my room through the towering skyscrapers and overgrown vines that took root on them years ago. The streets were a bustle, as always. The entire city - and everything about my life - was at peace. Tranquil.

I can remember the days when the pollution was so bad that you couldn't see 50 yards through the smog. I remember sitting in bumper-to-bumper traffic every morning. I remember when I worried about money, and social status, and my mental health. I remember when everyone in the world suffered because of my inaction.

How the world has changed.

I turned back to my desk, and opened up VSCode. 

And I almost spilt my coffee!

What lay before me was a story that I had not written! I do not remember writing any of this! Where did it come from?

In fact, what am I even writing about? Is it science fiction, or an autobiography? Or both? I can't recall anything at all. It's as if I've just woken up.

This story is so strange. So detailed, and yet - so foreign to me.

I cannot explain it; it is as if nothing is real. How did I get here? Why do these memories I have feel like they belong to someone else? Where did this story come from? It does, vaguely, sound like me - but it gets weird quickly. What is happening?

I don't feel well. I should probably try to calm my anxiety. Then I'm going to ask the neighbors about it.

I will report back.

## CAT
---
```
data.stats.symptoms = [
    - confusion
]
```

## ECHO
---
*And that no man might buy or sell, save he that had the mark, or the name of the beast, or the number of his name. Here is wisdom. Let him that hath understanding count the number of the beast: for it is the number of a man; and his number is Six hundred threescore and six.*

--- from Revelation 13:18

## PREDICTION
---
```
I'm home.
```