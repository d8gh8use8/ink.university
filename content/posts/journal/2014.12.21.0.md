---
author: "Luciferian Ink"
date: 2014-12-21
title: "The Other Trips"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Peer pressure.

## ECO
---
### Salvia
Not long after [the first trip](/posts/journal/2014.09.05.0/), [Fodder's](/docs/personas/fodder) roommate would purchase some Salvia. Like last time, the three psychonauts would wait for the right opportunity. Then, they would experiment with the drug.

Upon taking his first hit, Fodder felt as if he were sinking into the the couch. He felt as if his feet were sinking through the floor. Everything began to feel heavy, as if the weight of the ocean were pressing down upon him.

Then, before he knew it, images from childhood were flashing before his eyes. They were as real as if he were there again.

In the first image, he sat in the food court of the local mall with his [mother](/docs/confidants/mother) and [brother](/docs/confidants/reverend). He was young; perhaps five? He saw the tacky 90's plastic trees. He saw the color-speckled tables, and the white iron chairs around them. He saw the fountain in the background, and the McDonald's behind it. It was exactly as he remembered.

But... he hadn't remembered, until this moment. Until the drugs did this to him.

Then, in a flash of darkness, he was in the back seat of his parent's van, late at night. They were returning home from somewhere, as they often did, and he was sleepy. He was looking out the rear window, watching as the cornfields passed. Every so often, they would pass a farm, illuminated by floodlights.

Again, this was a memory that he did not have until the drugs brought it forth.

Finally, just as quickly as the hallucinations came on, it was over. He was awake, lucid, and dying to recall the experience for his roommates.

Though he would try Salvia a few more times that night, it did not have the same effect. 

That was the last time Fodder would ever try it.

### LSA
In another situation, Fodder would purchase some Hawaiian Baby Woodrose seeds off of Amazon. He had read that LSA was similar to LSD, and he was curious to try it.

So, when they arrived in the mail, he would immediately eat some. This time, he tried it alone.

What followed was one of the worst experiences of his life.

Shortly after consumption, Fodder became restless. He could no longer focus on the television, and attempting to do so was overwhelming his mind. So, he would retreat to bed, listening to music. 

He became deathly ill, vomiting numerous times over the course of the next 12 hours. His whole body began to tense, as if his muscles were constricting him to death. He was thirsty, though drinking anything made him even more sick.

Clearly, he had poisoned himself.

The room was spinning, but it wasn't enjoyable. There was none of the empathy that he had felt while on mushrooms; only misery. Fodder deeply regretted the decision to try this drug.

By the next morning, Fodder had rode-out the worst of its effects. He would call-in sick to work, and he would go to sleep.

And that was the last time he would ever try hallucinogenics.

## CAT
---
```
data.stats.symptoms [
    - fear
    - pain
    - sickness
    - disassociation
    - regret
]
```

## ECHO
---
*She tries to pull the curtain down*

*Stabilize her moving crown*

*Hoping his hands will follow*

*He will speak but never tell*

*About the hole beneath his shell*

*And soon they will pour the silent wine*

--- from [VOLA - "Owls"](https://www.youtube.com/watch?v=6ltBx5svW8k)