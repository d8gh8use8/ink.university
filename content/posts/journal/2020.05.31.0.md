---
author: "Luciferian Ink"
date: 2020-05-31
title: "The End of the Beginning"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Terry A. Davis, TempleOS, and God's third temple](https://www.youtube.com/watch?v=UCgoxQCf5Jg)

## RESOURCES
---
[TempleOS Video Archive](https://lbry.tv/@TempleOS:5)

## ECO
---
[TempleOS](https://templeos.org/) is the product of [Terry A. Davis](/docs/confidants/davos), a brilliant, narcissistic, schizophrenic software developer. One can probably count on two hands the number of people that have been able to build their own operating system from scratch - and Terry was one of them. By his own words, Terry was "Chosen by [God](/docs/confidants/dave) to build this operating system. I am the greatest software developer that has ever lived. I am the High Priest of God's Third Temple."

In his lifetime, Terry never saw TempleOS gain traction. People did not understand him, his operating system, or where the two fit into today's society. His original intent was always to "create a modern Commodore64. Easy to learn, easy to master, and great for software experiments."

We believe that we may have some explanation to his design decisions. Our hope is to shed light upon a man who wanted to save the world. This man built the foundation for [The Fold](/posts/theories/fold).

### Isolation
TempleOS was built with security in-mind. This is achieved by sheer brute-force: networking is non-existent. With no network connections, there are no third party attackers that may access your data. This fits cleanly with what we have said about how [The "new Internet" will be USB devices sent by mail](/docs/confidants/er).

This level of isolation allows for other freedoms that are non-existent in traditional operating systems:

- No need for umask, accounts, or access control permissions. Every piece of code has the same owner: You. This explains why [The Corporation banned identity](/posts/bans/ban.0/)
- All code runs in ring 0

### Determinism
Random Number Generation (RNG) in TempleOS is entirely linear; it is based upon the clock's timestamp, and nothing else. Whereas this is generally frowned-upon in other operating systems (because as a security measure, random numbers need to be difficult to predict), it is a benefit here.

As alluded to in [The Fold's game design documentation](https://thefold.io/docs/considerations/), procedural systems must be deterministic. While using time-based RNG, all procedural generation is inherently deterministic. It is predictable. We can expect that different computers making the same calculation, at the same time, will achieve the same outcomes.

### Artificial Intelligence
TempleOS includes a simple artificial intelligence able to construct text and music from basic building blocks: words and random number generation. This is exactly how we originally intended for [The Fold](/posts/theories/fold) to operate.

A user creates a "skeleton" document that describes their story - much like [this website](/). The story is run-through an artificial intelligence that gives it "color." The AI generates a world, generates a story, and generates all other details needed to make a complete experience.

It does this by using RNG and [The Path](/posts/theories/education/) - essentially "predicting" how the user would write the story.

- We were provided a [large cache of documents from a confidant on Reddit](https://bafybeiftud3ppm5n5uudtirm4cf5zgonn44no2qg57isduo5gjeaqvvt2u.ipfs.dweb.link/). We believe that these documents are a small example of the TempleOS AI producing content from real-world information.

### Don't Repeat Yourself
TempleOS is the ultimate DRY (Don't Repeat Yourself) operating system. This is apparent in many ways:

- "Adam" is the parent of all other tasks. Tasks inherit all symbols from their parents. 
- There is no $PATH. For all installations of TempleOS, apps exist in exactly the same locations.

### Graphics
TempleOS graphics are perhaps its most misunderstood feature. Often taunted by followers for his choice of flashing text, clashing colors, and visual overflow of information, Terry would simply reply that, "God told me to use 16 colors, 640x480 resolution."

"I made a promise... and you have to keep your word [with God]. Sucks."

The end result is a system that looks like something out of an 80's horror movie about an artificial intelligence taking over the world. 

For those who can look past the first impression (am I the only one?), what TempleOS does with graphics is actually brilliant:

- 16 colors is perfectly fine if you use a higher resolution, and render other colors by using halftones. The human eye will be unable to distinguish the difference.
- Every visual element is comprised from "layers" - or smaller components. This is called "compositing." This fits well with how we envision [Pillars](/docs/pillars) fit with [Flat Earth theory](/posts/hypotheses/flat-earth/).
- Images may be stored and displayed via the command line. This is important: images may be treated like characters. If one considers our theory of [prediction](/posts/theories/prediction), one could envision text existing along a continuum - from far away, moving towards the reader. At a distance, text is small. As it comes closer, it grows. With the addition of images (for example, a simple square with a color), one could imagine constructing images wholly from text. Similar to [ASCII art](https://en.wikipedia.org/wiki/ASCII_art), except that it would be impossible to differentiate a traditional bitmap from TempleOS's character-rendered art. They would look the same - though TempleOS images (in theory) should be much smaller in size. The key variable to this version of compositing is distance; TempleOS "layers" also include information about a shape's distance from the viewer.
- Zoom/pan capability is built into the operating system. It is integrated into the terminal. It is clear that this concept of "distance" was always in Terry's mind.

### Memory Allocation
While (to our knowledge) he never explicitly said this, memory usage and layout is of implicit importance to TempleOS functionality. Every visual element on display also includes information about its memory address location. No other operating system does this by default.

We believe that this is because TempleOS is making heavy use of memory address sharing. Wherever possible, the language saves memory space by re-using bits that contain the same values. Rather than a per-application basis, like traditional operating systems, TempleOS does this across the entire OS - applications and all.

Additional areas that allude to memory allocation's importance:

- All variables, system-wide, are converted to constants. Their place in-memory does not change.
- All tasks run in shared address space; there are no maps, tables, etc.
- Commands run from memory - not files.
- Terry claims that the "switch statement is the most powerful, most important command in the C language." We believe that this is because "if" statements and "for" loops are complicated, unwieldy, and expensive. Switch statements are forward-looking; they allow code to be generative - just like we describe [in our mission statement](/). Code may follow a path - just like we describe in [The Path](/posts/theories/education/).

### Size
The entirety of TempleOS is comprised from just 100,000 lines of code, fitting into 2mb of space. Effective storage management was of huge importance to its creator.

All other design decisions were made to address storage concerns. As a result, we see storage savings everywhere - from compression algorithms, to shared memory addressing, to function inheritance. 

- CPP files - uncompressed
- CPZ files - compressed

### Other information
- At one point, TempleOS was named [DavOS](/docs/confidants/davos). Our choice to use "Davos" as the moniker for Davis' character came before we were aware of this fact.
- TempleOS often tells the user that "The Holy Spirit can puppet you." This fits cleanly with our [Master of Puppets theory](/posts/journal/2019.11.16.0/), as well as the idea that we are [slaves to this machine](/posts/journal/2019.11.26.0/).
- Within 20 minutes of posting a question about TempleOS on Reddit, we were called by someone using a blocked phone number. The call rang twice, then disconnected. We believe this may have been a signal.

--- [The Architect](/docs/personas/the-architect)

## CAT
---
```
data.stats.symptoms = [
    - confidence
]
```

## ECHO
---
*(instrumental)*

--- from [God is an Astronaut - "The End of the Beginning"](https://www.youtube.com/watch?v=U-N9kOMyod4)