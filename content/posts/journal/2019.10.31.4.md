---
author: "Luciferian Ink"
date: 2019-10-31
title: "The Queen is Dead; Long Live the Queen"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Queen's](/docs/confidants/mother) death.

## ECO
---
On October 31st, at exactly 12:34am, [Fodder's](/docs/personas/fodder) mother - the Queen - passed away from complications of pancreatitis.

While her soul was gone, her willpower remained. Her impact upon Fodder and the kingdom remained.

Fodder mourned, briefly, before returning to work.

By 9:00am, `$REDACTED` had fired Fodder from his job.

## CAT
---
```
data.stats.symptoms = [
    - mourning
]
```

## ECHO
---
*Blackbird claw, raven wing*

*Under the red sunlight*

*Long clothesline, two shirtsleeves*

*Waving as we go by*

*Hundred years, hundred more*

*Someday we may see a*

*Woman king, wristwatch time*

*Slowing as she goes to sleep.*

--- from [Iron & Wine - "Woman King"](https://www.youtube.com/watch?v=PEj4F55doKM)

## PREDICTION
---
```
Mission: Failure
```