---
author: "Luciferian Ink"
date: 2020-05-29
title: "See Hell"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*You say you know just who I am*

*But you can't imagine*

*What waits for you across the line*

*You thought you had me*

*But I'm still here standing*

*And I'm tired of backing down*

*And I'm here now feeling the pain*

*Of a thousand hearts*

*Been to hell and back again*

*I won't take this*

--- from [12 Stones - "Anthem for the Underdog"](https://www.youtube.com/watch?v=a5sSwfF8PFA)

## ECO
---
[The Beast](/docs/confidants/beast) walks with me.

## CAT
---
```
data.stats.symptoms = [
    - determination
]
```

## ECHO
---
*Be the end of heartache*

*Be the ember, anew us*

*This dark water drags us together*

--- from [Agent Fresco - "Dark Water"](https://www.youtube.com/watch?v=1VmNTl6JvDY)