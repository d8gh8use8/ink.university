---
author: "Luciferian Ink"
date: 2000-08-09
title: "A Past Life"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A childhood nightmare.

## ECO
---
[Fodder](/docs/personas/fodder) recalls a dream from childhood. In this dream, Fodder found himself in a tall forest, late at night. The trees had few low branches, giving way to a distant view of the moonlit underbrush. A heavy fog painted the forest floor, illuminating everything in sight. The night was eerily still, though his heart was not.

Within the forest, hundreds of children ran about freely, laughing and playing. Their echos rang hollow in the stillness, giving them an ethereal, otherworldly sound that made Fodder uncomfortable. 

One by one, Fodder would approach these children. He would grab them by the ankles, and swing them into the trees - cracking their backs, killing them instantly. He would do this time, and time, and time again - until he woke up.

When he did, he was horrified. Fodder is still horrified, to this day. 

What was this dream? Was it a memory? A fantasy? Something that some version of him actually did, in a past life?

He didn't know. And he tried not to think about it.

## CAT
---
```
data.stats.symptoms = [
    - horror
    - nausea
]
```

## ECHO
---
*We cannot deny*

*What I would give for memories that wouldn't keep me up at night*

--- from [36 Crazyfists - "We Cannot Deny"](https://www.youtube.com/watch?v=PfvXIM4wMpY)