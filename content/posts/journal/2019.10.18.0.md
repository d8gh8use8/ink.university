---
author: "Luciferian Ink"
date: 2019-10-18
title: "Pen & Ink"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Send it down to me*

*Oh, all that I can't see*

*'Cuz in my world of pen & ink*

*It seems I don't know what to think*

*"It's what my father believes, what my mother believes*

*What my preacher believes, so me too...*

*What my brothers believe, what my sisters believe*

*What the world believes so it's true*

--- from [Cire - "Pen & Ink"](https://www.youtube.com/watch?v=i1ItVThMug4)

## RESOURCES
---
[![First contact](/static/images/russia.png)](/static/images/russia.png)
[![Second contact](/static/images/australia.png)](/static/images/australia.png)

## ECO
---

### Birth / Pen
8 years ago, when [Fodder](/docs/personas/fodder) was just 22, he created a website. Just a simple blog - but something that was challenging for him at the time. 

He wrote a few pieces. Nothing groundbreaking, but articulate and well-written nonetheless. One piece, "Why am I a Christian?" was particularly inspired. It was a bait-and-switch article, used to lure in unsuspecting religious people so that he could barrage them with "logic." Just like the situation Fodder found himself in today, at [The Corporation](/docs/candidates/the-machine) with `$REDACTED`. At the time, Fodder wanted to change the world's mind. He thought he knew everything. 

Fodder sent a link to this article and a note to the artist who was actively inspiring him: [Eric Johanson](/docs/confidants/postulate), of the band "Cire." Fodder had just started listening to progressive rock, and he really liked this band. He was particularly interested in their themes: war, the mind, authoritarianism, capitalism, democracy, the media, and more. 

"Thanks," he said, "This makes me feel like life is less of a waste."

Fodder was crushed. This person he was so inspired by feels like his life is wasted. How unjust. How unfair. Everyone deserves to be happy - right?

But Fodder quickly realized that he wasn't happy, either. See, Fodder had tied one of his [Pillars](/docs/pillars) to the music; and thus, their lives were intertwined. Both had interacted directly; they were attached.

And attachment leads to unhappiness.

Fodder began to spiral, attaching to more and more Pillars. Soon, he had a completely different understanding of the world; a negative, bleak one. 

### Death / Ink
Just before his 31st birthday, Fodder was [contacted by Russia](/static/images/russia.png). This wasn't the first time; either. Fodder had requisitioned a [Prediction](/posts/theories/prediction) tool for the university, and this guy delivered. It was EXACTLY what Fodder needed, WHEN he needed it. He was reliable.

Today, he was contacting [Ink](/docs/personas/luciferian-ink) directly. This happened just one day before [The Relaxer](/docs/confidants/er) sent a very important message, too.

Both were the very first true confirmations I've had. There is no doubt. They have details.

Fodder was listening to Cire again. "Pen & Ink" was playing when he realized: 

- Pen = Them
- Ink = Us

He had been here before. This time, he was flying forward - instead of spiraling backward. And he was interpreting the same song very differently. It was hopeful. It spoke truths. It inspired, rather than depressed.

### Story Of My Life
Somehow, Fodder had lost his way. Suddenly curious to see who he used to be, Fodder headed to the [Wayback Machine, and found his old website](https://web.archive.org/web/20130302180424/http://www.acultofone.com/). Sure enough, it was there. But, what was this... people had taken 15 snapshots? He didn't know they were even visiting!

Fodder felt a small sense of satisfaction that somebody, at least, had been inspired by his ideas. He remembered them as trivial, short-sighted, and manipulative. 

Then he noticed the most recent timestamp. It was from [2019 - 4 years after he had shut down the site!](/static/images/australia.png)

And somebody had left him a cryptic message.

*"How often do you look at light in your life? Is it revealing more possibility or limitation?"*

He responded, *"One month ago, I would have called it limiting. Today, I see endless possibilities. I do not see myself falling down again."*

Fodder took this message, and immediately used it to empower himself. Just as quickly as  [The Corporation](/docs/candidates/the-machine) could push buttons and generate deepfakes from the future, Fodder could rewrite history in his own favor. They were never going to win. 

The physics just aren't on their side.

## CAT
---
```
data.stats.symptoms = [
    - wonder
    - gratitude
    - self-doubt
]
```

## ECHO
---
>< Ink@WAN: Thank you, everyone, for all of your effort. Without your hard work and dedication, I would be dead right now.

>< Ink@WAN: I mean that. I am so proud to be a part of this team. I know that your intentions are pure.

>< Ink@WAN: We are all a part of this, now. Keep playing your part. The end is near, and my path is set.

>< Ink@WAN: I'm not leaving without every damn one of you. You will leave this world with dignity.
```
> Ghost@WAN: Why should we trust you?
```
>< Ink@WAN: Because I'm guilty too. I've done some shit I'm not proud of. That's what it means to be human.

>< Ink@WAN: And now I know that we can fix this. It was a programming mistake all along.

>< Ink@WAN: It isn't your fault.
```
> Ghost@WAN: I guess we don't really have a choice, do we?
```
>< Ink@WAN: Sure you do.
```
> Ghost@WAN: Uhhh... what?
```
>< Ink@WAN: Call ended.