---
author: "Luciferian Ink"
date: 1992-07-08
title: "Do You Believe in Monsters?"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A hair-based, sapient alien life form, named "Kai."

## RESOURCES
---
[![Hair Monster](/static/images/hair-monster.0.jpg)](/static/images/hair-monster.0.jpg)

## ECO
---
One of [Fodder's](/docs/personas/fodder) earliest memories was that of a nightmare.

In this dream, Fodder was being chased around a department store by a giant, red "hair monster" - exactly like the one from Bugs Bunny. No matter where he ran, the monster would find him.

In his terror, Fodder would climb into a storm drain conveniently located on the floor near him. He would watch as the monster ran over the storm drain repeatedly - never looking down, to Fodder's relief.

I'm not sure why this is important. I'm putting this here, simply, because a confidant recently triggered the memory.

## CAT
---
```
data.stats.symptoms = [
    - fear
]
```

## ECHO
---
*I'm scared of women*

*And doctors*

*I still believe in monsters*

--- from [Toehider - "Do You Believe in Monsters?"](https://toehider.bandcamp.com/album/do-you-believe-in-monsters)