---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: The Ark"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Dave's](/docs/confidants/dave) behavior.

## ECO
---
The Ark is to be a test of Fodder's "[juxtaposition](/posts/journal/2019.11.15.0/)" theory. It is to be constructed in Nashville, TN, and to be announced to a private audience on November 22-23, 2019, between 6pm and 10pm.

While Fodder can not stand The Dave - or his subordinate `$REDACTED` - he was going to place trust in the man, regardless. Truly, he was placing trust in Dave's army of empaths.

"Build me the heaven that your Bible promises," he demanded.

### Theme
The Ark is to be themed as The Dave wishes.

Even if that path includes [surrounding himself with young women, and forcing them to lure-in all competition - only to have them arrested by the FBI](/posts/journal/2019.11.06.0/).

### Mission
Prove to me that you aren't the monster everyone says you are.

Or, prove that you are. I don't give two shits about what happens to you.

You have one year.

### Other
Do not assist the Dave. He cannot be trusted.

## CAT
---
```
data.stats.symptoms = [
    - schadenfreude
]
```

## ECHO
---
*This is the tale of a bully and a lil' bitch. I'm talking about a mean, nasty bully, and a scrawny-ass lil' bitch.*

*This bully would pick on the lil' bitch every day after school. Lil' bitch had cash, lil' bitch got robbed. Lil' bitch bowed-up, lil' bitch got stomped.*

*The point I'm trying to make here is: that bully owned that lil' bitch like a Lincoln Continental. But, something happened to change it all.*

*On this... X-Mas of our tale... lil' bitch got lucky with the gifts. See, something lil' bitch loved more than anything - before family, God, and country - was baseball. He loved the shit out of it. Well, on this X-Mas morning, when he got out of his lil' bedroom, and got under his lil' tree, guess what he found?*

*An Easton Magnum aluminum baseball bat. Lil' bitch grabbed that bat, and went right outside to play. Didn't even take the bow off, he was so excited!*

*And there he was on the corner-ball field, hittin' balls like he was Sammy Sosa, when his bully comes up - eye'n that bat. And just as the bully made his way up to him, without any warning:*

*BLOOD! FUCKING EVERYWHERE!*

*Blood on the ground! Blood on lil' bitch! Blood dripping down the bow still wrapped-around that bat. Lil' bitch knocked every tooth out of that motherfucker's head.*

*Cracked his skull open. Popped his eyeball out on home plate.*

*All because lil' bitch saw the truth:*

*That bat may have been made to play ball, but that wasn't it's purpose.*

*Uh, no. In that moment - in lil' bitch's hands - it was there to break this nigga's head open.*

*See, Mr. Dave, we all have a purpose in this world. And it's our lot in life to reveal what this purpose is.*

*But you don't have to look no more. Cuz for you, that day has come. Want to know why?*

*I'm going to break `$REDACTED`. And you're going to be my aluminum bat.*

--- from [Lil' Bitch, Season 4 Episode 6 of Mr. Robot](https://www.esmailcorp.com/)

## PREDICTION
---
```
"I don't have kids," huh? We saw the picture sitting on your desk.
```