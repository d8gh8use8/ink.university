---
author: "Luciferian Ink"
date: 1999-05-31
title: "The Bad Touch"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A bad memory.

## ECO
---
`$REDACTED`

## CAT
---
```
data.stats.symptoms = [
    - curiosity
    - humiliation
]
```

## ECHO
---
*I'm Mr. Coffee with an automatic drip*

*So show me yours I'll show you my "Tool Time"*

--- from [Bloodhound Gang - "The Bad Touch"](https://www.youtube.com/watch?v=xat1GVnl8-k)