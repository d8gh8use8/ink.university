---
author: "Luciferian Ink"
date: 2019-10-31
title: "Revenge of the Narcissist"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
["Revenge on the Narcissist? How Karma works"](https://www.youtube.com/watch?v=pIE_NDSzpm0)

## ECO
---
Who's behind HER mask?

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - certainty
]
```

## ECHO
---
*Dan ​VS. ​egg. ​It’s ​not ​that ​hard ​to ​get.*

*There’s ​Dan ​seated ​there ​with ​the ​egg ​at ​a ​6-sided ​table ​(and ​the ​table’s ​not ​set)*

*Under ​the ​violent ​moon, ​I ​guess ​they’ll ​be ​starting ​soon.*

*He ​writes ​me ​a ​note, ​it ​says ​“please ​enclose ​a ​Hanna-Barbera ​cartoon”*

*“Are ​you ​gonna ​fight ​it?”*

*“I ​don’t ​know”*

*Or ​is ​it ​more ​a ​mind ​thing? ​Keen ​to ​see ​where ​all ​this ​goes*

*A ​TV ​set ​inside ​plays ​“Hawaiian ​Eye”*

*I ​wish ​I ​could ​tell ​Dan ​good ​luck, ​but ​I ​fear ​that ​too ​much ​time ​has ​ticked ​by*

*We ​wear ​a ​face ​the ​same; ​a ​smile ​but ​deep ​in ​pain*

*For ​the ​chairs ​have ​a ​press-stud, ​but ​it ​always ​undoes*

*So ​we ​sit ​on ​the ​cold ​wire ​frame*

*“Are ​you ​gonna ​fight ​him?”*

*“I ​don’t ​know”*

*Or ​is ​it ​like ​a ​mind ​sting, ​wonder ​how ​this ​will ​go*

*“How ​you ​gonna ​find ​him?”*

*“I ​don’t ​know!”*

*Or ​is ​it ​more ​reminding, ​revealing, ​revolting?*

*“No.”*

*Oh! ​Dan ​is ​egg.*

*This ​is ​how ​it ​goes*

*He ​leans ​in ​and ​rubs ​his ​finger ​back ​and ​forth ​under ​his ​nose*

*The ​end.*

*(dan ​is ​also ​egg)*

--- from [Toehider - "Dan vs. Egg"](https://toehider.bandcamp.com/track/dan-vs-egg)

## PREDICTION
---
```
Mission: Success
```