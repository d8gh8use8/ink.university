---
author: "Luciferian Ink"
date: 2019-10-08
title: "Song of I"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*The suit was fitted pressed and laid out on the bed*

*The folders stacked upon the shelf*

*And every detail was immaculately cared for*

*A corporation of yourself*

*And soon they'll all have their opinions*

*And every expert has a voice*

*You've brought them all here today through charismatic speaking*

*Without so much a pretense why*

*And when it's all laid out the tension has been broken*

*Until a moment passes by*

--- from [Tub Ring - "The Charismatic Smile"](https://www.youtube.com/watch?v=Sg24Dgl0HuI)

## ECO
---
On Tuesday, October 8th, 2019, we released our research - this website - to the world. While we knew that it was rough, lacking detail and evidence to support many of the claims, we could wait no longer. Something told us that it had to be today.

By 10:00am, [The Corporation](/docs/candidates/the-machine) had a response. They were announcing a re-branding, and a massive downsizing.

Somehow, they knew that [Fodder](/docs/personas/fodder) would release his work today. And they were prepared for the transition.

They were accepting him.

## CAT
---
```
data.stats.symptoms = [
    - egotism
]
```

## ECHO
---
*And that's when you smile and take control of the room*

*You smile, now they're staring at you*

*Well you've raised the bar by playing rough*

*But are your methods good enough*

*To take the fools for everything they're worth*

--- from [Tub Ring - "The Charismatic Smile"](https://www.youtube.com/watch?v=Sg24Dgl0HuI)