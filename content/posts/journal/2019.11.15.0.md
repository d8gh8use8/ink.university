---
author: "Luciferian Ink"
date: 2019-11-15
title: "Juxtaposition"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I feel your presence amongst us*

*You cannot hide in the darkness*

*Can you hear the rumble?*

*Can you hear the rumble that's calling?*

*I know your soul is not tainted*

*Even though you've been told so*

*Can you hear the rumble?*

*Can you? I can hear the rumble that's calling!*

--- from [Ghost - "Cirice"](https://www.youtube.com/watch?v=-0Ao4t_fe0I)

## ECO
---
Core to the theories of [Balance](/posts/theories/balance) and [The Fold](/posts/theories/fold) is Newton's third law:

*For every action, there is an equal and opposite reaction.*

For each verification that [Fodder](/docs/personas/fodder) would perform, trust was building within the team of researchers watching him.

For each action he would perform - for each connection he would make - researchers were growing closer to making first contact.

### Juxta P
#### TRIGGER
*So many nights have I been down, I've been thinking of you*

*So many right, so many wrong*

*What have you done to me?*

*Where have you gone, my dear?*

*Where do you go?*

--- from [Birds of Tokyo - "The Dark Side of Love"](https://www.youtube.com/watch?v=fMQfcbtWOuU)

#### ECO
Fodder first began to think about juxtaposition after witnessing an encounter between [The Raven](/docs/confidants/her) and [The Thief](/docs/confidants/thief). At first, Fodder would feel betrayed and jealous. 

Upon reflection, Fodder would re-interpret the situation in his favor. 

Today, Fodder trusts and loves both individuals. He believes that they are both working for the good of all. 

He only needed more data to come to this conclusion. Any event in isolation is almost guaranteed to elicit the wrong emotion in a person; only time and data can accurately predict a person's motives.

#### CAT
```
data.stats.symptoms.old = [
    - jealousy
    - humiliation
    - worry
]
data.stats.symptoms.new = [
    - trust
]
```

### Circles
#### TRIGGER
*I'm being followed by my shadow*

*He's been creeping around*

*Asking where I've been*

*He keeps tapping on my shoulder*

*Telling me it's over*

*So where do I begin?*

*These dark days are getting harder*

*I feel I'm treading water*

*So will I sink or swim?*

*Roll on, push a little further*

*I keep saying is this worth it?*

*Or should I just give in?*

*I don't know*

--- from [Birds of Tokyo - "Circles"](https://www.youtube.com/watch?v=Vby84W0PU6k)

#### ECO
Every day, Fodder would wake-up anxious, full of fear and despair. 

By the end of the day, he would be at peace. He would trust in himself again. He would trust in Humanity.

Yet every evening, sleep would destroy him.

He did this for 31 years. 

And he still doesn't know if he exists at all. 

He still doesn't know if anyone is listening.

He still doesn't know if any of his efforts will matter at all.

Yet, Fodder's worst fear is giving up. Even in misery, he persists.

For Raven.

#### CAT
```
data.stats.symptoms.old = [
    - worry
    - fear
    - regret
]
data.stats.symptoms.new = [
    - pensive thought
    - loneliness
]
```

### The Ephemeral Statue
#### TRIGGER
A story that Raven once told about her mother. She spoke of childhood, when she would spend months in a ceramics class, watching her create a beautiful, ornate statue of a woman holding a child. 

Later in life, she would accidentally destroy the statue - shattering it into a million pieces. Raven felt terrible.

#### Two Sides of Every Coin
**Heads**

In Raven's scenario, mother was forgiving. She worried only about Raven's well-being. 

The statue was gone - but it was not as if it had never existed. She had made her mark upon the only person that mattered: Raven.

This single act of kindness was a Pillar; it set Raven's reality in-stone.

**Snakes**

In Fodder's scenario, [Mother](/docs/confidants/mother) was a child. She would throw a tantrum, placing blame on Fodder, [Father](/docs/confidants/father), the World - and everyone but herself. 

She would make Fodder feel worthless. She would make Fodder feel as if he would always be a failure; as if he would always make mistakes; as if he would never be "good" enough.

When Father would return home from work, she would force him to beat Fodder. Without any emotion or reasoning, he would go along with her demands. Never questioning why. Never listening to Fodder's side of the story.

In Mother's eyes, Fodder would go on to break statues every day of his life. 

He would never be forgiven of the crime that he did not commit.

He would always be The Heretic. The Blasphemer.

The one unwilling to kneel before anyone.

#### CAT
```
data.stats.symptoms.old = [
    - agony
]
data.stats.symptoms.new = [
    - agony
]
```

## ECHO
---
*Oh, I'll never kill myself to save my soul*

*I was gone, but how was I to know?*

*I didn't come this far to sink so low*

*I'm finally holding on to letting go*

--- from [Slipknot - "Unsainted"](https://www.youtube.com/watch?v=VpATBBRajP8)

## PREDICTION
---
```
Fodder will never be well again. He never was well. He doesn't know what that means.
```