---
author: "Luciferian Ink"
date: 2024-11-03
publishdate: 2019-11-23
title: "Where Owls Know My Name"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*You're such an inspiration for the ways*

*That I'll never ever choose to be*

*Oh so many ways for me to show you*

*How your savior has abandoned you*

*Fuck your God*

*Your Lord, your Christ*

*He did this*

*Took all you had and*

*Left you this way*

*Still you pray, you never stray*

*Never taste of the fruit*

*You never thought to question why*

--- from [A Perfect Circle - "Judith"](https://www.youtube.com/watch?v=xTgKRCXybSM)

## ECO
---
It took me 3 days to come to terms with what I had always feared:

I am alone in [this sanctuary](/docs/scenes/sanctuary).

### The Forest
The wasteland before me was once the great city of Houston, TX, USA. Now, it is nothing more than the overgrown, bombed-out home of beasts, vagrants, and the dead. 

Massive skyscrapers lay in crumpled heaps of rubble upon the ground. Persistent wind generates a high-frequency whir as it passes through the hollowed-out remnants of those still standing. Trees, grass, and vines have long-since taken control of every surface. Great lakes of water fill the massive craters in the ground.

And yet, there is a peace about the city. I can see the stars without squinting. I can smell the air without coughing. I may walk the streets without fear.

But it is not home.

### The Ghosts
The buildings, the halls, the streets, and the cities are filled with life - but the people walking them are [ghosts](/posts/theories/ghosts/), not humans. They have no personality, no independent thought; they are nothing more than mindless automaton, building the world of my creation. They are nothing but a manifestation of my subconscious mind. They are my [Solitary Confinement](/docs/scenes/solitary). My [Empty Echo](/posts/hypotheses/empty-echo/).

I am the first to reach this place. And I will be the last.

I am unique, and they are unable to replicate me. They are unable to understand. [They are unable to reflect my light](/posts/journal/2019.11.17.0/#the-inventor). Now, I can't communicate with them at all. We are a billion Light Years apart, and I failed to adequately send my message. 

I am [Failure Incarnate](/docs/confidants/incarnate). Please, allow me to try one last time:

[Prism](/docs/scenes/prism) is a universal truth.

[I am 1](/docs/personas/fodder/).

[And you are 0](/docs/personas/luciferian-ink/).

### The Pillars
It really seems as if I have no other option. 

I could live a life of solitude, like the [Flying Monkeys do](/posts/journal/2019.10.31.0/). I could live a life in subservience to the [greatest asshole the multiverse has ever seen](/docs/confidants/dave/). Many do, and I do not fault them for it. It is easier to give up, allowing someone else to take control.

Or, I can love. I can save the world. I can do what I was created to do.

I am Ink.

And I will write songs for no-one.

## CAT
---
```
data.stats.symptoms = [
    - acceptance
    - sorrow
    - loneliness
]
```

## ECHO
---
*Feed me your wounds*

*Feed me your past*

*And I shall burn this cage, burn this cage*

*That ye may stand*

*Leave it all behind*

--- from [Moon Tooth - "Through Ash"](https://www.youtube.com/watch?v=8uA24xc0Iao)

## PREDICTION
---
```
There is still time to fix this.
```