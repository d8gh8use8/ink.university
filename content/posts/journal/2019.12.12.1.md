---
author: "Luciferian Ink"
date: 2019-12-12
title: "The Mark"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Australian rock.

## ECO
---
Scoping out the room, [Malcolm](/docs/personas/fodder) would notice a man sitting alone at the periphery. His face was painted by a large birthmark, and his demeanor was that of a loner. He was silent, not engaging with the other patients at all. Every few minutes, he would rise from his seat, make his way to the phone, and call somebody. He was upset, and he wanted out of here - just like Malcolm did.

As the evening set, Malcolm would make his way over to sit next to the man. 

"What's your name?" Malcolm asked.

"`$REDACTED`," he replied. "But you can call me [The A-System](/docs/confidants/a-system)."

"I'm Malcolm. But you can call me [Ink](/docs/personas/luciferian-ink)."

"Nice to meet you," he replied, clearly being cautious. "So... the police brought you here, huh?"

Malcolm caught his nervousness, and tried to reassure him, "It's all a big misunderstanding. Don't worry, I wouldn't hurt a fly."

"Okay, good," he said, visibly relaxing.

Malcolm would go on to have deep and engaging conversation with this man. About his situation. About his theories. About philosophy. About psychology. About computers. About mental health and the prison system. About religion. About music. The two would sit conversing until it was bed time. The A-System was a great listener, and great at reflecting ideas back to Malcolm - but he didn't speak very much at all. Malcolm quickly learned that this man was an empath, and that he felt deeply for other people.

He would also learn that this man had been diagnosed with Borderline Personality Disorder and Dissociative Identity Disorder.

"You can also call me Ashlynn, or Asriel," he said, laughing.

"You hide it well," Malcolm replied. "I haven't noticed a switch at all."

"It's funny, though. I have multiple identities, too. At least, in my story, I do. There's three: Fodder, [The Architect](/docs/personas/the-architect), and Ink. It's not the same as DID though; I can clearly differentiate between my reality and fiction."

"I can too," he replied. "I am always present in all of my aliases."

"Interesting," Malcolm responded. "You know, I get the impression that you know more than you're letting on. Just a hunch, but in the back of my mind, I'm wondering if you were put here to speak with me. Any chance you're FBI?"

"I was put here because I tried to kill myself on pills."

"Well, then, perhaps I was put here to speak to you," Malcolm reflected. He had the strongest impression that this was his target. This was The Mark. This was the person who desperately needed Malcolm to plant a seed of hope. "Something feels strange, right? Something about this situation feels important. Maybe the most important thing that will ever happen to us. Do you feel it?"

"I feel it," he responded. "I've been here before. This time is different."

"You and me. We're going to do something incredible," Malcolm replied.

## CAT
---
```
data.stats.symptoms [
  - empathy
]
```

## ECHO
---
*They only thought something was up when his mailbox overflowed*

*A month or so of letters scattered all over the road*

*They found him on a Monday, inside his house alone*

*With no suspicious circumstances, cause of death unknown*

*Said Joan "Oh Bill, he was my Dad. He told me back in June"*

*He began to shake and nervously that William left the room*

*And the next day, well, she found him hanging, swinging in the breeze*

*With a letter of confession in the pocket of his jeans*

*"I don’t know how to think too much*

*The glue would sting as the tongue did touch*

*I saw you two and I jumped the gun*

*I can’t handle the guilt o’er what I done"*

--- from [Toehider - "The Guy That No-One Really Knows"](https://toehider.bandcamp.com/album/i-like-it)