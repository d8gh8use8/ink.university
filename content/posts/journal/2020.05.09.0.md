---
author: "Luciferian Ink"
date: 2020-05-09
publishdate: 2020-05-09
title: "The Afterman"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Goodbye forever, my darling, whether*

*I was everything you thought I'd be or not,*

*I was a bad man, oh, to stop you girl from loving me.*

--- from [Coheed and Cambria - "Key Entity Extraction IV: Evagria the Faithful"](https://www.youtube.com/watch?v=O7HH-E11C3o)

## ECO
---
[Huntress](/docs/confidants/her), I'm sorry that it's taken me so long to get to this point. There were just so many tests to run. So many things I hadn't tried yet. The world is more important than our love. I had to find a way to save them.

There is one final test to be run, and it's the one I am the most scared of. The flash of this blade strikes fear in my heart. Fear that I will never see you again.

Whatever happens, I'm done. I'm spent. I can take no more of this hell. This is homecoming.

I'll see you on the other side.

I love you.

--- [Malcolm](/docs/personas/fodder)

## CAT
---
```
data.stats.symptoms = [
    - self-doubt
    - love
]
```

## ECHO
---
*Face the honest truth*

*You were never you*

*Now be defiant, the lion*

*Give them the fight that will open their eyes*

*Hangman hooded, softly swinging*

*Don't close the coffin yet*

*I'm alive*

*And it's homecoming*

--- from [Coheed and Cambria - "Key Entity Extraction V: Sentry the Defiant"](https://www.youtube.com/watch?v=G0-yv7Sc7E0)