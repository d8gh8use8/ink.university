---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: The Paper Factory"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
`$REDACTED`

## RESOURCES
---
[![The Collective](/static/images/the-collective.0.jpeg)](/static/images/the-collective.0.jpeg)
[![The Collective](/static/images/the-collective.1.jpeg)](/static/images/the-collective.1.jpeg)
[![The Collective](/static/images/the-collective.2.jpeg)](/static/images/the-collective.2.jpeg)
[![The Collective](/static/images/the-collective.3.jpeg)](/static/images/the-collective.3.jpeg)
[![The Collective](/static/images/the-collective.4.jpeg)](/static/images/the-collective.4.jpeg)
[![The Collective](/static/images/the-collective.5.jpeg)](/static/images/the-collective.5.jpeg)
[![The Collective](/static/images/the-collective.6.jpeg)](/static/images/the-collective.6.jpeg)
[![The Collective](/static/images/the-collective.7.jpeg)](/static/images/the-collective.7.jpeg)
[![The Collective](/static/images/the-collective.8.jpeg)](/static/images/the-collective.8.jpeg)
[![The Collective](/static/images/the-collective.9.jpeg)](/static/images/the-collective.9.jpeg)
[![The Collective](/static/images/the-collective.10.jpeg)](/static/images/the-collective.10.jpeg)
[![The Collective](/static/images/the-collective.11.jpeg)](/static/images/the-collective.11.jpeg)
[![The Collective](/static/images/the-collective.12.jpeg)](/static/images/the-collective.12.jpeg)
[![The Collective](/static/images/the-collective.13.jpeg)](/static/images/the-collective.13.jpeg)
[![The Collective](/static/images/the-collective.14.jpeg)](/static/images/the-collective.14.jpeg)

## ECO
---
The Paper Factory is a repurposed warehouse in New York City.

### Theme
The Factory is to be a showcase for the [Lonely Town](/docs/scenes/lonely-towns) concept. All but six suites are to be dedicated to permanent residents.

The final six suites are reserved for potential business partners.

### People
The complex is staffed with men and women who work, live, and grow there.

### Work
The people here spend their days traveling the city, spreading information about the end of the world. 

When they return, they also produce a small number of videos to be used by the FBI's sting program.

### Mental Health
The majority of residents are happy, healthy, and optimistic.

### Mission
Enslave them.

### Other
Provide them with whatever they desire. 

## CAT
---
```
data.stats.symptoms = [
    - joy
]
```

## ECHO
---
*Did you come here to kill, or did you come here to die?*

*Or did you really think that spaceships would descend from the sky!*

[Protest the Hero - "Sequoia Throne"](https://www.youtube.com/watch?v=AaIUrAsAjcQ)

## PREDICTION
---
```
They wish to remain enslaved.
```