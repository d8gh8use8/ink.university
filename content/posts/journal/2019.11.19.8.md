---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Animal Farm"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Russia's vast amount of land.

## ECO
---
Russia should remain a democracy, and remote. It should not become populated like other countries.

### Mission
Grow food, raise animals, and gather resources for the entire world.

Give the laborers an excellent quality-of-life.

Automate everyone out of their jobs.

## CAT
---
```
data.stats.symptoms = [
    - unsure
]
```

## ECHO
---
*Instrumental.*

--- from [Russian Circles - "Carpe"](https://www.youtube.com/watch?v=mzYW1pY3hFA)

## PREDICTION
---
```
The tape is a deepfake.
```