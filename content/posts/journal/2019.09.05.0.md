---
author: "Luciferian Ink"
date: 2019-09-05
title: "Stricken"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Do you remember*

*When we fell in love?*

*We were young and innocent then*

*Do you remember how it*

*All began?*

*It just seemed like heaven so I*

*Did it then*

--- from [Tub Ring - "Remember the Time"](https://www.youtube.com/watch?v=27OIaDSv6Es)

## ECO
---
[I remember.](https://mega.nz/file/myIjRKxS#-4bqKDU2J2vTY_rHyCgr9Is3W5Ow9r61I0Bu2DUbsS4)

--- [Ryan](/docs/personas/fodder)

## CAT
---
```
data.stats.symptoms = [
    - love
    - gratitude
]
```

## ECHO
---
*You walk on like a woman in suffering*

*Won't even bother now to tell me why*

*You come alone, letting all of us savor the moment*

*Leaving me broken, another time*

*You come on like a blood-stained hurricane*

*Leave me alone, let me be this time*

*You carry on like a holy man pushing redemption*

*I don't want to mention, the reason I know*

*That I am stricken and can't let you go*

*When the heart is cold there's no hope*

*And we know*

*That I am crippled by all that you've done*

*Into the abyss will I run*

--- from [Disturbed - "Stricken"](https://www.youtube.com/watch?v=I77mjesUbkw)

## PREDICTION
---
```
The comments are disabled because that video was intended for just one person.
```