---
author: "Luciferian Ink"
date: 2003-02-28
title: "Ghost in the Wire"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A call from [Grandmother](/docs/confidants/grandmother). She swears that she did not initiate it.

## ECO
---
[Fodder](/docs/personas/fodder) was watching television one evening with his family. The phone rang. So, [Father](/docs/confidants/father) answered it.

"Hello?" he asked. 

"Hello?" the caller responded.

"Who is this?" 

"Uh... who is this?"

"You called me!" Father responded, "Who is this?"

"No I didn't!" they protested, "You called me. Wait... Jim, is that you?"

"Mom?" 

"It is you! How are you doing, son? What were you calling about?"

"Things are great! I didn't call you, though. You called me!"

"No I didn't!" Grandmother responded.

"Yes you did!" he said, laughing, "You're starting to lose that memory with age!"

Grandmother laughed in reply, "I swear! I didn't call you! My phone rang!"

"Well so did ours."

"How strange," she replied.

"Very," he responded.

## CAT
---
```
data.stats.symptoms = [
    - confusion
]
```

## ECHO
---
*Now that you've felt my existence*

*I'm not in my house but yours*

*Listening for footsteps in bedrooms, opening your cabinets and doors*

*I have named this stance "distance"*

*Observant, a fly on your wall*

*She sits and listens to whispers of his secret telephone call*

--- from [Vultress - "A Chord from Heaven"](https://vultress.bandcamp.com/track/a-chord-from-heaven-i)

## PREDICTION
---
```
Grandfather's AI was the broker of that phone call.
```