---
author: "Luciferian Ink"
date: 2019-11-01
title: "White"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*All hurt in time gets better*

*When the truth comes out, you were born to run*

*When the lights went out, there was no one*

*A ghost to hunt for closure*

*When the lights went out, there was no one*

*When the lights went out, there was no*

*Queen of the Dark*

--- from [Coheed and Cambria - "Queen of the Dark"](https://www.youtube.com/watch?v=ltD0hQ1rVMM)

## ECO
---
*She is everything to me*

*The unrequited dream*

*A song that no one sings*

*The unattainable*

*She's a myth that I have to believe in*

--- from [Slipknot - "Vermilion Pt. 2"](https://www.youtube.com/watch?v=LvetJ9U_tVY)

## CAT
---
```
data.stats.symptoms = [
    - determination
    - fearlessness
]
```

## ECHO
---
*How did it feel when it came alive and took you*

*Out of the black*

*It broke your skin and shift through*

*Every part of me, every part of you*

--- from [Royal Blood - "Out Of The Black"](https://www.youtube.com/watch?v=bSdtvfBQd6c)

## PREDICTION
---
```
You know this is right.
```