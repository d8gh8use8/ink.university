---
author: "Luciferian Ink"
date: 2019-06-24
title: "A Slow Descent into Madness"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Picture yourself by yourself*

*No tomorrow or yesterday*

*Drowning in your own decay*

*You'll never be alone this time*

*So follow the trend break or bend*

*It's your funeral-long goodbye*

*Shut tight an open eye*

*You'll never be alone*

--- from [The Butterfly Effect - "A Slow Descent"](https://www.youtube.com/watch?v=HcfwXUyudG8)

## RESOURCES
---
[![Offer of Employment - Fodder](/static/images/offer-of-employment.png)](/static/images/offer-of-employment.png)

## ECO
---
Today was [Fodder's](/docs/personas/fodder) first day with [The Corporation](/docs/candidates/the-machine). He had been hired as a Senior DevOps Engineer to lead a small, focused task-force of developers in the adoption of automation within the prison system. He couldn't wait to get started!

Though, he was wary. He knew within the first 5 minutes of interviewing that his boss, `$REDACTED`, was a narcissist. 

It would be interesting to see how this plays-out.

A narc reporting to a narc. Poetic.

## CAT
---
```
data.stats.symptoms = [
    - wariness
]
```

## ECHO
---
*Last night I dreamt I held you with me*

*Close enough to feel you breathe*

*When I awoke I lay here empty*

*Caught between the want and need*

*Now in the darkness I am only*

*Thoughtful hopes and pieces mind*

*These dreams are all I've ever wanted*

*Found behind the closing eyes*

*How long we've been trying to reach you*

*We all fall down like this sometimes*

*Trying to reach you*

*I'm trying*

--- from [The Butterfly Effect - "Reach"](https://www.youtube.com/watch?v=c6dlgtH8ZtE)