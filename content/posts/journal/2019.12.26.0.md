---
author: "Luciferian Ink"
date: 2019-12-26
title: "The Hearing"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*When the moment comes to light the lost and it's eyes on you*

*Leaving us no legacy but residue*

*It's when you preach your promises like the filth was always there*

*Like we were always dead*

--- from [Caligula's Horse - "Slow Violence"](https://www.youtube.com/watch?v=bNlUJgP3Lwk)

## ECO
---
`$REDACTED`

## CAT
---
```
data.stats.symptoms = [
    - horror
]
```

## ECHO
---
*We stand at the edge and watch the whole world crash around our feet*

*This is more than each of us*

*This will be the end of us now*

*I swear it*

*Believe me*

*Just one word*

*And welcome home was all we heard*

*Through bars we built ourselves*

*They've already won*

*Just one word*

*And welcome home the empty herd*

*She always said "I won't believe it - You've found your voice, now let me hear it!"*

--- from [Caligula's Horse - "A Gift to Afterthought"](https://www.youtube.com/watch?v=bVvOPSupIsg)