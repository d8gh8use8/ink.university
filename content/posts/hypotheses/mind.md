---
author: "Luciferian Ink"
title: Mind
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Mind
Classification: Theory
Stage: Hypothesis
```

## ECO
---
We are all narcissists.

We perceive external objects (people and environment) through our own lens ([Perspective](/posts/theories/perspective)).

DNA and perspective will create the illusion of an environment that traces your entire lineage. When conflict arises, the internal object always wins.

Once a person masters this understanding, nothing scares them.

## ECHO
---
*Keep holding on*

*When my brain's tickin' like a bomb*

*Guess the black thoughts have come again to get me*

*Sweet bitter words*

*Unlike nothing I have heard*

*Sing along mockingbird*

*You don't affect me*

--- from [Korn - "Coming Undone"](https://www.youtube.com/watch?v=NFSJE9pYLbU)