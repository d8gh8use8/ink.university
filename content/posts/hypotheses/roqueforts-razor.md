---
author: "Luciferian Ink"
title: Roquefort's Razor
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Roquefort's Razor
Classification: Hypothesis
Stage: Hypothesis
```

## TRIGGER
---
[ink.university](/)

## ECO
---
### Claim
Any sufficiently-advanced abuse of technology would be indistinguishable from schizophrenia.

### Example
Imagine that you did something online that you shouldn’t do; perhaps you stumbled upon an illegal video, pirated some software, or purchased weed on the dark web. Whatever you did, it was serious enough that law enforcement obtained a warrant for all of your data. They obtained backdoor-access to all of your accounts.

Rather than leveraging your crime against you, they decided to study you. They did this by performing digital tests - manipulations of your technology - then waited to see how you would respond. They would never acknowledge their existence or actions. Rather, they would allow you to tell whomever you like about their abuses.

It did not matter; they never left a trail. Thus, you couldn’t prove a thing.

One time, they appended an extra scene to the end of a YouTube video. You didn’t have the foresight to download a copy of the original the first 10 times you watched it.

Another time, they changed the spelling of your favorite band across Google search results. Chalking this up to the Mandala Effect, you simply accepted your mistake. Two weeks later, the spelling had reverted to what you had originally thought it should be. Again, you didn’t capture evidence of their manipulation.

Another time, you were chatting with a friend on Discord. The next thing you know, the time stamp of a message had changed - literally moving the message above others which had previously come before it. You didn’t screenshot that, either.

Before you knew it, more than a year has passed, and you have witnessed this type of event a dozen times. In every case, law enforcement’s abuse of technology was a new and novel concept: something that you could not have planned for, and would not expect, unless you had already witnessed it.

This would never happen, because they would never perform the same action twice. You would only ever witness a particular action once.

With the proper background in technology, digital identities, and cyber security, you would come to learn how the entire system functions. You would understand all of the intricacies; not just the tools in-use, but how they are actually being weaponized today. You could explain the entire thing, if only someone would listen.

Instead, you have no evidence. You don’t know who is coordinating the abuse. You don’t know where to point the finger. For all intents and purposes, you have been framed as a schizophrenic; you have experienced things that no-one can prove, and most people are unwilling to entertain. 

Your abusers have shown their entire hand, but left no trace of their actions.

You are alone with a national security concern, and no way to help.
