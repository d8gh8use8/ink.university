---
author: "Luciferian Ink"
title: Conspiracy Theories
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Conspiracy Theories
Classification: Theory
Stage: Hypothesis
```

## ECO
---
Conspiracy theories are just regular theories without evidence.

A lack of evidence does not mean that something is untrue.

## ECHO
---
*Possibly I've seen too much*

*Hangar 18, I know too much*

--- from [Megadeth - "Hangar 18"](https://www.youtube.com/watch?v=x5A9Aa-WU5E)
