---
author: "Luciferian Ink"
date: 2020-09-01
title: "Sleep"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
September 5, 2020.

## ECO
---
Humanity is running out of time to make meaningful change. Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from sleeping.

Every waking hour must be spent fixing this mess that we have created.

## CAT
---
```
data.stats.symptoms [
    - exhaustion
]
```