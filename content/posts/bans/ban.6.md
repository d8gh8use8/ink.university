---
author: "Luciferian Ink"
date: 2020-01-01
publishdate: 2020-01-01
title: "Freedom"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) living situation.

## ECO
---
Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from living alone.

## CAT
---
```
data.stats.symptoms [
    - unease
]
```