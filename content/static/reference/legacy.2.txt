Author date: 2012-05-07

Plot
1. Summary
   1. Book exists in a plausible future Earth, where the world awaits a pending meteor shower while governments evacuate as many people as possible. The protagonist is a corporate spy that comes into the knowledge that the means to create matter from nothing has been discovered – effectively giving the ability to play god. This knowledge is being kept under wraps as corporations grow rich bleeding humanity dry, when it could be used to generate the necessary materials to evacuate OR save the world. To expose this information, the protagonist must make hard decisions regarding morality in order for the greater good.
2. Prologue
   1. Protagonist smokes a cigarette, while reflecting on his past addiction. He mourns for the people that will be lost on Earth, while simultaneously looking for a way to help. He feels hopeless and depressed.
      1. Setting needs to be described in detail
   2. Chapter 1
      1. Protagonist is on a mission to clean up sensitive materials
         1. Left over from a downed ship
            1. This JUST happened, and is major news. Protagonist is panicked
         2. Notices an irradiated butterfly
            1. glowing
            2. reflects on the Canadian situation
            3. 3 wings
      2. Protagonist finds an artifact that leads him to believe that he is being left in the dark.
         1. Probably the shrapnel/electronics from a military weapon.
            1. Made of an unknown material.
      3. Chapter 2
         1. Protagonist takes the artifact to Old Guy X
            1. Old Guy X cannot determine what the device is
            2. Old Guy X has a Contact within Corporation Z
            3. Protagonist needs to find a way into Corporation Z
         2. Old Guy X will call in a favor
            1. Protagonist needs a job within the corp to gain clearance with the contact
            2. Protagonist will need to place backdated applications in Corporation Z
               1. They have a lengthy background check process
         3. Chapter 3
            1. Protagonist prepares to infiltrate Corporation Z
               1. Stakes out the building
                  1. Notes when major players come and go
                  2. Distracts one person leaving for lunch, takes access badge
                  3. Dresses like this person
            2. Enters the building
               1. On cell phone, in a hurry
               2. When questioned by security, he completely blows them off and walks past
                  1. Of course this causes trouble, but since he is able to provide an access badge, they don't study it closely and let him by
                  2. He threatens them with their jobs
            3. Inside the building
               1. Now inside, Protagonist must remain hidden from as many people as possible
                  1. He is quite obviously not in the right place to them
               2. Smooth talks a pretty young receptionist into getting him into the applications department
                  1. Once inside, he has to find a way to plant his pre-approved resume
                  2. Is presented with a dilema
                     1. Place the resume on a desk and hope somebody will find it
                     2. Or, place resume on a computer and risk getting caught
                        1. He opts for this, and does indeed set off alarms
                        2. Must quickly remove evidence and go with the paper method anyway
            4. Exiting the building
               1. Is exiting the building when Protagonist is stopped by security for questioning
                  1. Protagonist only has a few minutes to get out before “real” person he is impersonating comes back from lunch
               2. By a stroke of luck, the Contact/Love Interest comes into the room
                  1. Identifies the Protagonist as the real person
                  2. Escorts him out
               3. Gives him an item for Old Guy X just before leaving
                  1. Device to remotely upload scanned data to her
               4. Drops keycard on the ground just outside, and heads for work at Corporation X
                  1. “It's going to have to wait, Old Man, I've got a session scheduled for this afternoon.”
            5. Chapter 4
               1. Heads to HQ to interrogate a corporate spy
                  1. This is a voluntary interrogation
                  2. Turns out to be the same person he impersonated
                  3. This person has come to Corporation X for help
                     1. He believes that he is being followed by his own company, Corporation Z
               2. Epilogue
3.